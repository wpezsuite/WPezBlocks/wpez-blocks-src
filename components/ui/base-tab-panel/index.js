/**
 * External dependencies
 */
import classnames from 'classnames';
import { partial, noop, find } from 'lodash';
import defaults from "../../attributes/base/base-button-group-icon-buttons/defaults";

/**
 * WordPress dependencies
 */
// import { Component } from '@wordpress/element';
const {Component, Fragment} = wp.element;
// import { withInstanceId } from '@wordpress/compose';
const {withInstanceId} = wp.compose;

/**
 * Internal dependencies
 */
// import { NavigableMenu } from '../navigable-container';
const {NavigableMenu} = wp.components;
// import Button from '../button';
const {Button} = wp.components;

const TabButton = ( { tabId, onClick, children, selected, disabled, ...rest } ) => (
    <Button role="tab"
            tabIndex={ selected ? null : -1 }
            aria-selected={ selected }
            id={ tabId }
            onClick={ onClick }
            disabled={disabled}
            { ...rest }
    >
        { children }
    </Button>
);

class TabPanel extends Component {
    constructor() {
        super( ...arguments );
        const { tabs, initialTabName } = this.props;

        this.handleClick = this.handleClick.bind( this );
        this.onNavigate = this.onNavigate.bind( this );

        this.state = {
            selected: initialTabName || ( tabs.length > 0 ? tabs[ 0 ].name : null ),
        };
    }

    handleClick( tabKey ) {
        const { onSelect = noop } = this.props;
        this.setState( {
            selected: tabKey,
        } );
        onSelect( tabKey );
    }

    onNavigate( childIndex, child ) {
        child.click();
    }

    render() {
        const { selected } = this.state;
        const {
            activeClass = 'is-active',
            className,
            instanceId,
            orientation = 'horizontal',
            tabs,
        //    initialTabName,
            disabled
        } = this.props;

        let selectedTab = find( tabs, { name: selected } );

        if ( typeof selectedTab === 'undefined' || selectedTab.disabled === true ){

            for (let i = 0; i < tabs.length; i++) {
                if (typeof tabs[i] !== 'undefined' && tabs[i].disabled !== true ){
                    selectedTab = tabs[i];
                    this.setState( {
                        selected: tabs[i].name,
                    } );
                    break;
                }
            }
        }

        // something has gone wrong
        if ( typeof selectedTab === 'undefined'){
            return '';
        }

        const thisDisabled = (disabled) ? Array.isArray(disabled) ? disabled : [] : [];
        const selectedId = instanceId + '-' + selectedTab.name;

        return (

            <div className={ className }>

                <NavigableMenu
                    role="tablist"
                    orientation={ orientation }
                    onNavigate={ this.onNavigate }
                    className="components-tab-panel__tabs"
                >
                    { tabs.map( ( tab ) => (

                        <TabButton className={ classnames( tab.className, { [ activeClass ]: tab.name === selected } ) }
                                   tabId={ instanceId + '-' + tab.name }
                                   aria-controls={ instanceId + '-' + tab.name + '-view' }
                                   selected={ tab.name === selected }
                                   disabled={ (tab.disabled) ? tab.disabled : thisDisabled.includes(tab.name) }
                                   key={ tab.name }
                                   onClick={ partial( this.handleClick, tab.name ) }
                        >
                            { tab.title }
                        </TabButton>
                    ) ) }
                </NavigableMenu>
                { selectedTab && (
                    <div aria-labelledby={ selectedId }
                         role="tabpanel"
                         id={ selectedId + '-view' }
                         className="components-tab-panel__tab-content"
                         tabIndex="0"
                    >
                        { this.props.children( selectedTab ) }
                    </div>
                ) }
            </div>
        );
    }
}

export default withInstanceId( TabPanel );