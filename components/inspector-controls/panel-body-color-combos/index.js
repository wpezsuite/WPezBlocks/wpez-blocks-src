/**
 * External dependencies
 */
/*
import map from 'lodash/map';
import classnames from 'classnames';
*/

/**
 * Internal dependencies
 */
import defaults from '../../defaults';
import config from './config';

import ButtonGroupColorCombos from "../../attributes/button-group-color-combos";


/**
 * WordPress dependencies
 */

const {__, sprintf} = wp.i18n;
const {Component, Fragment} = wp.element;
const {PanelBody } = wp.components;

/**
 * Inspector controls
 */
class PanelBodyColorCombos extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        const {
            wpezBC,
            attributes,
            setAttributes,
        } = this.props;

        const thisBC = wpezBC.get('inspector__' + this.constructor.name);
        const thisConfig = config(thisBC);

        const {
            featureSet = '01',
            colorComboBasic,
            colorCombo
        } = attributes;

        return (
            <Fragment>
                <PanelBody
                    title={thisConfig.panelBodyTitle}
                    className={defaults.panelBodyClassName}
                    initialOpen={thisConfig.initialOpen}
                >

                    { (thisConfig.attribute__colorComboBasic.active && featureSet === '01') &&

                    <ButtonGroupColorCombos
                        {...thisConfig.attribute__colorComboBasic}
                        value={colorComboBasic}
                        onChange={value => setAttributes({colorComboBasic: value})}
                    />
                    }

                    { (thisConfig.attribute__colorCombo.active && featureSet > '01') &&
                    <ButtonGroupColorCombos
                        {...thisConfig.attribute__colorCombo}
                        value={colorCombo}
                        onChange={value => setAttributes({colorCombo: value})}
                    />
                    }

                </PanelBody>

            </Fragment>
        );
    }
}

export default PanelBodyColorCombos;
