import map from "lodash/map";

function config(args = {} ){

    if ( typeof args === 'undefined' || typeof args !== 'object'){
        args = {};
    }

    let obj = {};

    const attrs = [
        'colorComboBasic',
        'colorCombo'
    ];

    map(attrs, (attr, i) => {
        obj['attribute__' + attr] = {};
        obj['attribute__' + attr].active = true;
    });

    obj.panelBodyTitle = args.panelBodyTitle || 'Colors';
    obj.initialOpen = args.initialOpen || false;

    obj.attribute__colorComboBasic.buttons = [
        {
            buttonText: 'Zero',
            buttonTextActive: true,
            ariaLabel: 'Combo: White + Black',
            tooltipText: 'White BG with Black text',
            // tooltipPosition: , << optional
            key: 'combo-0',
            value: 'combo-0',
            combo: [ '#FFFFFF', '#000000' ],
            // default: bool,  << only specify once
            // disabled: bool, << optional
            // className:
        },

        {
            buttonText: 'One',
            buttonTextActive: true,
            ariaLabel: 'Combo: Black + White',
            tooltipText: 'Black BG with White text',
            key: 'combo-1',
            value: 'combo-1',
            combo: [ '#000000', '#FFFFFF'  ],
        },
    ];
    obj.attribute__colorCombo.buttons = [
        {
            buttonText: 'Two',
            buttonTextActive: true,
            ariaLabel: 'Combo: Black + Black + White',
            tooltipText: 'Black + Black + White',
            // tooltipPosition: , << optional
            key: 'combo-2',
            value: 'combo-2',
            combo: [ '#000000','#000000', '#FFFFFF'  ],
            // default: bool,  << only specify once
            // disabled: bool, << optional
            // className:
        },

        {
            buttonText: 'Three',
            buttonTextActive: true,
            ariaLabel: 'Combo: Grey + Black + White',
            tooltipText: 'Grey + Black + White',
            key: 'combo-3',
            value: 'combo-3',
            combo: [ '#555d66','#000000', '#FFFFFF'  ],
        },

        {
            buttonText: 'Four',
            buttonTextActive: true,
            ariaLabel: 'Combo: White + Grey + White',
            tooltipText: 'Grey + White + Black',
            key: 'combo-4',
            value: 'combo-4',
            combo: [ '#555d66', '#FFFFFF', '#000000' ],
        },
    ];

    // -- merge
    map(attrs, (attr, i) => {

        // only merge is args.* === object
        if ( typeof args['attribute__' + attr] === 'object' ){
            obj['attribute__' + attr] = {...obj['attribute__' + attr], ...args['attribute__' + attr]};
        }

    });

    if ( typeof obj.attribute__colorComboBasic.buttons  !== 'object' ){
        obj.attribute__colorComboBasic.buttons = {};
    }

    // no buttons? then de-active
    if  ( obj.attribute__colorComboBasic.buttons.length < 1 ){
        obj.attribute__colorComboBasic.active = false;
    }

    //

    if ( typeof obj.attribute__colorCombo.buttons  !== 'object' ){
        obj.attribute__colorCombo.buttons = {};
    }

    // no buttons? then de-active
    if  ( obj.attribute__colorCombo.buttons.length < 1 ){
        obj.attribute__colorCombo.active = false;
    }

    return obj;

}

export default config;

