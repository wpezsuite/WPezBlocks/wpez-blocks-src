const attributes = {

    colorComboBasic: {
        type: "string",
    },

    colorCombo: {
        type: "string",
    }
};

export default attributes;