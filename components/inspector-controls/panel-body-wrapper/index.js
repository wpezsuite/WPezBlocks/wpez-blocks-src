/**
 * External dependencies
 */
/*
import map from 'lodash/map';
import classnames from 'classnames';
*/

/**
 * Internal dependencies
 */
import defaults from '../../defaults';
import config from './config';

import TabPanel from "../../ui/base-tab-panel";
import TextControlAnchor from "../../attributes/text-control-anchor";
import ButtonGroupBlockWidth from "../../attributes/button-group-block-width";
import ButtonGroupAlignHorizontal from "../../attributes/button-group-align-horizontal";
import ButtonGroupAlignVertical from "../../attributes/button-group-align-vertical";
// import ButtonGroupSizes from '../../attributes/button-group-sizes';
import ButtonGroupElementHeight from "../../attributes/button-group-element-height";
import RangePickerWidthColumns from "../../attributes/range-picker-width-columns";

import SelectAOSAimations from "../../attributes/select-animate-on-scroll-animations";
import SelectAnimateCSS from "../../attributes/select-animate-css";
import ButtonGroupTimeDuration from "../../attributes/button-group-time-duration";
import ButtonGroupTimeDelay from "../../attributes/button-group-time-delay";
import SelectAOSEasing from "../../attributes/select-animate-on-scroll-easing";
import RangePickerAnimateIterations from "../../attributes/range-picker-animate-iterations";
import RadioPositionVertical from "../../attributes/radio-position-vertical";
import ButtonGroupAnimateOffset from "../../attributes/button-group-animate-offset";






/**
 * WordPress dependencies
 */
const {__, sprintf} = wp.i18n;
const {Component, Fragment} = wp.element;
const {PanelBody} = wp.components;



/**
 * Inspector controls
 */
class PanelBodyWrapper extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        const {
            wpezBC,
            attributes,
            setAttributes,
        } = this.props;

        const thisBC = wpezBC.get('inspector__' + this.constructor.name);
        const thisConfig = config(thisBC);

        const {

            featureSet = '01',

            blockWidth,
            blockHeight,
            blockAnchor,

            boxWidth,
            boxHeight,
            boxAlign,
            boxAlignVert,

            blockAniName,
            blockAniDuration,
            blockAniDelay,
            blockAniIterations,

            blockAniEasing,
            blockAniEleAnchor,
            blockAniWinAnchor,
            blockAniOffset,

            boxAniName,
            boxAniDuration,
            boxAniDelay,
            boxAniIterations,


            boxAniEasing,
            boxAniEleAnchor,
            boxAniWinAnchor,
            boxAniOffset,

        } = attributes;


        const tabs = defaults.tabsWrapper;
        tabs[1].disabled = false;
        if (featureSet !== '99'){
            tabs[1].disabled = true
        }

        return (
            <Fragment>

                <PanelBody
                    title={thisConfig.panelBodyTitle}
                    className={defaults.panelBodyClassName}
                    initialOpen={thisConfig.initialOpen}
                >

                    <TabPanel className={defaults.tabPanelClassname}
                              activeClass="tab-active"
                              initialTabName="setup"
                              tabs={tabs}>
                        {

                            (tab) => {

                                if ( 'setup' === tab.name ) {
                                    return (

                                        <div className={defaults.tabPanelInnerWrapperClassname}>

                                        {(thisConfig.attribute__blockWidth.active) &&
                                        <ButtonGroupBlockWidth
                                            {...thisConfig.attribute__blockWidth}
                                            value={blockWidth}
                                            onChange={value => setAttributes({blockWidth: value})}

                                        />
                                        }

                                            {(thisConfig.attribute__blockHeight.active && featureSet > '01') &&
                                            <ButtonGroupElementHeight
                                                {...thisConfig.attribute__blockHeight}
                                                value={blockHeight}
                                                onChange={value => setAttributes({blockHeight: value})}
                                            />
                                            }


                                            {(thisConfig.attribute__blockAnchor.active) &&
                                            <TextControlAnchor
                                                {...thisConfig.attribute__blockAnchor}
                                                value={blockAnchor}
                                                onChange={value => setAttributes({blockAnchor: value})}
                                            />
                                            }

                                            { ( featureSet > '01' ) &&

                                                <Fragment>

                                            <div className={"wpez-blocks-divider"}></div>

                                            {(thisConfig.attribute__boxWidth.active) &&
                                                <RangePickerWidthColumns
                                                {...thisConfig.attribute__boxWidth}
                                                value={boxWidth}
                                                onChange={value => setAttributes({boxWidth: value})}

                                                />
                                            }

                                            {(thisConfig.attribute__boxHeight.active) &&
                                                <ButtonGroupElementHeight
                                                {...thisConfig.attribute__boxHeight}
                                                value={boxHeight}
                                                onChange={value => setAttributes({boxHeight: value})}
                                                />
                                            }

                                            {(thisConfig.attribute__boxAlign.active
                                                && !thisConfig.attribute__boxAlign.removeWhenWidth.includes(boxWidth)) &&
                                                <ButtonGroupAlignHorizontal
                                                {...thisConfig.attribute__boxAlign}
                                                value={boxAlign}
                                                onChange={value => setAttributes({boxAlign: value})}

                                                />
                                            }

                                            {( ( blockHeight !== 'none' ) && thisConfig.attribute__boxAlignVert.active
                                                && !thisConfig.attribute__boxAlignVert.removeWhenHeight.includes(boxHeight)) &&
                                                <ButtonGroupAlignVertical
                                                {...thisConfig.attribute__boxAlignVert}
                                                value={boxAlignVert}
                                                onChange={value => setAttributes({boxAlignVert: value})}

                                                />
                                            }
                                                </Fragment>
                                            }

                                        </div>
                                    )

                                } else {
                                    return (

                                        <div className={defaults.tabPanelInnerWrapperClassname}>

                                            {thisConfig.attribute__blockAniName.active &&
                                            <SelectAnimateCSS
                                                {...thisConfig.attribute__blockAniName}
                                                value={blockAniName}
                                                onChange={value => setAttributes({blockAniName: value})}
                                            />
                                            }

                                            {false && thisConfig.attribute__blockAniName.active &&
                                            <SelectAOSAimations
                                                {...thisConfig.attribute__blockAniName}
                                                value={blockAniName}
                                                onChange={value => setAttributes({blockAniName: value})}
                                            />
                                            }

                                            {(!thisConfig.attribute__blockAniDuration.removeOthers.includes(blockAniName)) &&
                                            <Fragment>

                                                {(thisConfig.attribute__blockAniDuration.active) &&
                                                <ButtonGroupTimeDuration
                                                    {...thisConfig.attribute__blockAniDuration}
                                                    value={blockAniDuration}
                                                    onChange={value => setAttributes({blockAniDuration: value})}
                                                />
                                                }

                                                {(!thisConfig.attribute__blockAniDuration.removeOthers.includes(blockAniDuration)) &&
                                                <Fragment>

                                                    {(thisConfig.attribute__blockAniDelay.active) &&
                                                    <ButtonGroupTimeDelay
                                                        {...thisConfig.attribute__blockAniDelay}
                                                        value={blockAniDelay}
                                                        onChange={value => setAttributes({blockAniDelay: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__blockAniIterations.active) &&
                                                    <RangePickerAnimateIterations
                                                        {...thisConfig.attribute__blockAniIterations}
                                                        value={blockAniIterations}
                                                        onChange={value => setAttributes({blockAniIterations: value})}

                                                    />
                                                    }

                                                    {(false && thisConfig.attribute__blockAniEasing.active) &&
                                                    <SelectAOSEasing
                                                        {...thisConfig.attribute__blockAniEasing}
                                                        value={blockAniEasing}
                                                        onChange={value => setAttributes({blockAniEasing: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__blockAniEleAnchor.active) &&
                                                    <RadioPositionVertical
                                                        {...thisConfig.attribute__blockAniEleAnchor}
                                                        value={blockAniEleAnchor}
                                                        onChange={value => setAttributes({blockAniEleAnchor: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__blockAniWinAnchor.active) &&
                                                    <RadioPositionVertical
                                                        {...thisConfig.attribute__blockAniWinAnchor}
                                                        value={blockAniWinAnchor}
                                                        onChange={value => setAttributes({blockAniWinAnchor: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__blockAniOffset.active) &&
                                                    <ButtonGroupAnimateOffset
                                                        {...thisConfig.attribute__blockAniOffset}
                                                        value={blockAniOffset}
                                                        onChange={value => setAttributes({blockAniOffset: value})}

                                                    />
                                                    }

                                                </Fragment>
                                                }
                                            </Fragment>
                                            }

                                            <div className={"wpez-blocks-divider"}></div>

                                            {thisConfig.attribute__boxAniName.active &&
                                            <SelectAnimateCSS
                                                {...thisConfig.attribute__boxAniName}
                                                value={boxAniName}
                                                onChange={value => setAttributes({boxAniName: value})}
                                            />
                                            }

                                            { false && thisConfig.attribute__boxAniName.active &&
                                            <SelectAOSAimations
                                                {...thisConfig.attribute__boxAniName}
                                                value={boxAniName}
                                                onChange={value => setAttributes({boxAniName: value})}
                                            />
                                            }

                                            {(!thisConfig.attribute__boxAniDuration.removeOthers.includes(boxAniName)) &&
                                            <Fragment>

                                                {(thisConfig.attribute__boxAniDuration.active) &&
                                                <ButtonGroupTimeDuration
                                                    {...thisConfig.attribute__boxAniDuration}
                                                    value={boxAniDuration}
                                                    onChange={value => setAttributes({boxAniDuration: value})}
                                                />
                                                }

                                                {(!thisConfig.attribute__boxAniDuration.removeOthers.includes(boxAniDuration)) &&
                                                <Fragment>

                                                    {(thisConfig.attribute__boxAniDelay.active) &&
                                                    <ButtonGroupTimeDelay
                                                        {...thisConfig.attribute__boxAniDelay}
                                                        value={boxAniDelay}
                                                        onChange={value => setAttributes({boxAniDelay: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__boxAniIterations.active) &&
                                                    <RangePickerAnimateIterations
                                                        {...thisConfig.attribute__boxAniIterations}
                                                        value={boxAniIterations}
                                                        onChange={value => setAttributes({boxAniIterations: value})}

                                                    />
                                                    }

                                                    {(false && thisConfig.attribute__boxAniEasing.active) &&
                                                    <SelectAOSEasing
                                                        {...thisConfig.attribute__boxAniEasing}
                                                        value={boxAniEasing}
                                                        onChange={value => setAttributes({boxAniEasing: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__boxAniEleAnchor.active) &&
                                                    <RadioPositionVertical
                                                        {...thisConfig.attribute__boxAniEleAnchor}
                                                        value={boxAniEleAnchor}
                                                        onChange={value => setAttributes({boxAniEleAnchor: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__boxAniWinAnchor.active) &&
                                                    <RadioPositionVertical
                                                        {...thisConfig.attribute__boxAniWinAnchor}
                                                        value={boxAniWinAnchor}
                                                        onChange={value => setAttributes({boxAniWinAnchor: value})}

                                                    />
                                                    }


                                                    {(thisConfig.attribute__boxAniOffset.active) &&
                                                    <ButtonGroupAnimateOffset
                                                        {...thisConfig.attribute__boxAniOffset}
                                                        value={boxAniOffset}
                                                        onChange={value => setAttributes({boxAniOffset: value})}

                                                    />
                                                    }

                                                </Fragment>
                                                }
                                            </Fragment>
                                            }
                                        </div>
                                    )
                                }
                            }
                        }
                    </TabPanel>

                </PanelBody>

            </Fragment>
        );
    }
}

export default PanelBodyWrapper;
