const attributes = {

    // -- block
    blockWidth: {
        type: "string"
    },
    blockHeight: {
        type: "string"
    },
    blockAnchor: {
        type: "string"
    },

    // -- box
    boxWidth: {
        type: "string"
    },
    boxHeight: {
        type: "string"
    },
    boxAlign: {
        type: "string"
    },
    boxAlignVert: {
        type: "string"
    },

    // -- ani block
    blockAniName: {
        type: "string"
    },
    blockAniDuration: {
        type: "string"
    },
    blockAniDelay: {
        type: "string"
    },
    blockAniIterations: {
        type: "string"
    },

    // --
    blockAniEasing: {
        type: "string"
    },

    blockAniEleAnchor:{
        type: "string"
    },
    blockAniWinAnchor:{
        type: "string"
    },
    blockAniOffset: {
        type: "string"
    },

    // -- ani box
    boxAniName: {
        type: "string"
    },
    boxAniDuration: {
        type: "string"
    },
    boxAniDelay: {
        type: "string"
    },
    boxAniIterations: {
        type: "string"
    },
    // --
    boxAniEasing: {
        type: "string"
    },

    boxAniEleAnchor:{
        type: "string"
    },
    boxAniWinAnchor:{
        type: "string"
    },
    boxAniOffset: {
        type: "string"
    }
};

export default attributes;