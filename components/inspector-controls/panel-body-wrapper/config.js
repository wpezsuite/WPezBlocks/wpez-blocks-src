import attrs from "./attributes";

function config(args = {}) {

    if (typeof args === 'undefined' || typeof args !== 'object') {
        args = {};
    }

    let obj = {};


    for (var attr in attrs) {
        obj['attribute__' + attr] = {};
        obj['attribute__' + attr].active = true;
    }

    obj.panelBodyTitle = args.panelBodyTitle || 'Wrapper';
    obj.initialOpen = args.initialOpen || false;

    obj.attribute__blockWidth.label = 'Block Width';
    obj.attribute__blockWidth.ariaLabel = obj.attribute__blockWidth.label;

    obj.attribute__blockHeight.label = 'Block Height';
    obj.attribute__blockHeight.ariaLabel = obj.attribute__blockHeight.label;

    obj.attribute__blockAnchor.label = 'Block HTML Anchor';
    obj.attribute__blockAnchor.ariaLabel = obj.attribute__blockAnchor.label;

    obj.attribute__boxWidth.label = 'Box Width (in Columns)';
    obj.attribute__boxWidth.ariaLabel = obj.attribute__boxWidth.label;

    obj.attribute__boxHeight.label = 'Box Height';
    obj.attribute__boxHeight.ariaLabel = obj.attribute__boxHeight.label;

    obj.attribute__boxAlign.label = 'Box Align';
    obj.attribute__boxAlign.ariaLabel = obj.attribute__boxAlign.label;

    obj.attribute__boxAlignVert.label = 'Box Align Vertical';
    obj.attribute__boxAlignVert.ariaLabel = obj.attribute__boxAlignVert.label;

    // block animate
    obj.attribute__blockAniName.label = 'Block Animate';
    obj.attribute__blockAniName.ariaLabel = obj.attribute__blockAniName.label;

    obj.attribute__blockAniDuration.label = 'Block Duration';
    obj.attribute__blockAniDuration.ariaLabel = obj.attribute__blockAniDuration.label;

    obj.attribute__blockAniDelay.label = 'Block Delay';
    obj.attribute__blockAniDelay.ariaLabel = obj.attribute__blockAniDelay.label;

    obj.attribute__blockAniIterations.label = 'Block Iterations';
    obj.attribute__blockAniIterations.ariaLabel = obj.attribute__blockAniIterations.label;

    obj.attribute__blockAniEasing.label = 'Block Easing';
    obj.attribute__blockAniEasing.ariaLabel = obj.attribute__blockAniEasing.label;

    obj.attribute__blockAniEleAnchor.label = 'Block Edge';
    obj.attribute__blockAniEleAnchor.ariaLabel = obj.attribute__blockAniEleAnchor.label;

    obj.attribute__blockAniWinAnchor.label = 'Block Edge Intersects Window';
    obj.attribute__blockAniWinAnchor.ariaLabel = obj.attribute__blockAniWinAnchor.label;

    obj.attribute__blockAniOffset.label = 'Block Offset';
    obj.attribute__blockAniOffset.ariaLabel = obj.attribute__blockAniOffset.label;

    // box animate
    obj.attribute__boxAniName.label = 'Box Animate';
    obj.attribute__boxAniName.ariaLabel = obj.attribute__boxAniName.label;

    obj.attribute__boxAniDuration.label = 'Box Duration';
    obj.attribute__boxAniDuration.ariaLabel = obj.attribute__boxAniDuration.label;

    obj.attribute__boxAniDelay.label = 'Box Delay';
    obj.attribute__boxAniDelay.ariaLabel = obj.attribute__boxAniDelay.label;

    obj.attribute__boxAniEasing.label = 'Box Easing';
    obj.attribute__boxAniEasing.ariaLabel = obj.attribute__boxAniEasing.label;

    obj.attribute__boxAniEleAnchor.label = 'Box Edge';
    obj.attribute__boxAniEleAnchor.ariaLabel = obj.attribute__boxAniEleAnchor.label;

    obj.attribute__boxAniWinAnchor.label = 'Box Edge Intersects Window xxxx';
    obj.attribute__boxAniWinAnchor.ariaLabel = obj.attribute__boxAniWinAnchor.label;

    obj.attribute__boxAniOffset.label = 'Box Offset';
    obj.attribute__boxAniOffset.ariaLabel = obj.attribute__boxAniOffset.label;

    // -- merge
    for (attr in attrs) {
        // only merge is args.* === object
        if (typeof args['attribute__' + attr] === 'object') {
            obj['attribute__' + attr] = {...obj['attribute__' + attr], ...args['attribute__' + attr]};
        }
    }

    // --- validation --

    // attribute__blockWidth

    // attribute__blockHeight
    if (typeof obj.attribute__blockHeight.exclude === 'undefined' || !Array.isArray(obj.attribute__blockHeight.exclude)) {
        obj.attribute__blockHeight.exclude = ['xxs', 'xxl'];
    }

    // attribute__blockAnchor

    // attribute__boxWidth

    // attribute__boxHeight
    if (typeof obj.attribute__boxHeight.exclude === 'undefined' || !Array.isArray(obj.attribute__boxHeight.exclude)) {
        obj.attribute__boxHeight.exclude = ['xxs', 'xxl'];
    }

    // attribute__boxAlign
    if (typeof obj.attribute__boxAlign.removeWhenWidth === 'undefined' || !Array.isArray(obj.attribute__boxAlign.removeWhenWidth)) {
        obj.attribute__boxAlign.removeWhenWidth = ['12'];
    }

    // attribute__boxAlignVert

    if (typeof obj.attribute__boxAlignVert.removeWhenHeight === 'undefined' || !Array.isArray(obj.attribute__boxAlignVert.removeWhenHeight)) {
        obj.attribute__boxAlignVert.removeWhenHeight = ['full'];
    }

    // attribute__blockAniName
    if (typeof obj.attribute__blockAniName.removeOthers === 'undefined'
        || !Array.isArray(obj.attribute__blockAniName.removeOthers)) {
        obj.attribute__blockAniName.removeOthers = ['none'];
    }

    // attribute__blockAniDuration
    if (typeof obj.attribute__blockAniDuration.removeOthers === 'undefined'
        || !Array.isArray(obj.attribute__blockAniDuration.removeOthers)) {
        obj.attribute__blockAniDuration.removeOthers = ['none'];
    }

    // attribute__blockAniDelay

    // attribute__boxAniName

    if (typeof obj.attribute__boxAniName.removeOthers === 'undefined'
        || !Array.isArray(obj.attribute__boxAniName.removeOthers)) {
        obj.attribute__boxAniName.removeOthers = ['none'];
    }

    // attribute__boxAniDuration
    if (typeof obj.attribute__boxAniDuration.removeOthers === 'undefined'
        || !Array.isArray(obj.attribute__boxAniDuration.removeOthers)) {
        obj.attribute__boxAniDuration.removeOthers = ['none'];
    }

    // attribute__boxAniDelay

    // attribute__boxAniDuration
    if (typeof obj.attribute__blockAniWinAnchor.defaultValue === 'undefined' ) {
        obj.attribute__blockAniWinAnchor.defaultValue = 'bottom';
    }

    return obj;

}

export default config;

