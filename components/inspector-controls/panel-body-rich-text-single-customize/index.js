/**
 * External dependencies
 */
/*
import map from 'lodash/map';
import classnames from 'classnames';
*/

/**
 * Internal dependencies
 */
import defaults from '../../defaults';
import config from './config';

import TabPanel from "../../ui/base-tab-panel";
import ButtonGroupHeading from "../../attributes/button-group-heading";
import SelectListStyleType from "../../attributes/select-list-style-type";
import ButtonGroupTextAlign from "../../attributes/button-group-text-align";
import ButtonGroupTextAlignVertical from "../../attributes/button-group-text-align-vertical";
import ButtonGroupSizes from '../../attributes/button-group-sizes';

import SelectAOSAimations from "../../attributes/select-animate-on-scroll-animations";
import SelectAnimateCSS from "../../attributes/select-animate-css";
import ButtonGroupTimeDuration from "../../attributes/button-group-time-duration";
import ButtonGroupTimeDelay from "../../attributes/button-group-time-delay";
import SelectAOSEasing from "../../attributes/select-animate-on-scroll-easing";
import RangePickerAnimateIterations from "../../attributes/range-picker-animate-iterations";
import RadioPositionVertical from "../../attributes/radio-position-vertical";
import ButtonGroupAnimateOffset from "../../attributes/button-group-animate-offset";


/**
 * WordPress dependencies
 */
const {__, sprintf} = wp.i18n;
const {Component, Fragment} = wp.element;
const {PanelBody} = wp.components;
const {ToggleControl} = wp.components;

/**
 * Inspector controls
 */
class PanelBodyRichTextSingleCustomize extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        const {
            rtsType,
            wpezBC,
            attributes,
            setAttributes,
        } = this.props;

        const thisBC = wpezBC.get('inspector__' + this.constructor.name);
        const thisConfig = config(thisBC);

        const {

            featureSet = '01',

            boxHeight,

            textTagName,
            textListStyleType,
            textDropCap,
            textSize,
            textAlign,
            textAlignVert,

            contentAniName,
            contentAniDuration,
            contentAniDelay,
            contentAniIterations,
            contentAniEasing,
            contentAniEleAnchor,
            contentAniWinAnchor,
            contentAniOffset,

        } = attributes;

        const tabs = defaults.tabsContent;
        tabs[1].disabled = false;
        if (featureSet !== '99') {
            tabs[1].disabled = true
        }
        return (
            <Fragment>

                <PanelBody
                    title={thisConfig.panelBodyTitle}
                    className={defaults.panelBodyClassName}
                    initialOpen={thisConfig.initialOpen}
                >

                    <TabPanel className={defaults.tabPanelClassname}
                              activeClass="tab-active"
                              initialTabName="content"
                              tabs={tabs}>
                        {
                            (tab) => {
                                if ('content' === tab.name) {
                                    return (

                                        <div className={defaults.tabPanelInnerWrapperClassname}>

                                            {(rtsType === 'head' && thisConfig.attribute__textTagName.active) &&
                                            <ButtonGroupHeading
                                                {...thisConfig.attribute__textTagName}
                                                value={textTagName}
                                                onChange={value => setAttributes({textTagName: value})}
                                            />
                                            }

                                            {(rtsType === 'list' && thisConfig.attribute__textListStyleType.active) &&
                                            <SelectListStyleType
                                                {...thisConfig.attribute__textListStyleType}
                                                value={textListStyleType}
                                                onChange={value => setAttributes({textListStyleType: value})}
                                            />
                                            }

                                            {(thisConfig.attribute__textAlign.active) &&
                                            <ButtonGroupTextAlign
                                                {...thisConfig.attribute__textAlign}
                                                value={textAlign}
                                                onChange={value => setAttributes({textAlign: value})}

                                            />
                                            }

                                            {( ( boxHeight !== 'none') && thisConfig.attribute__textAlignVert.active && featureSet > '01') &&
                                            <ButtonGroupTextAlignVertical
                                                {...thisConfig.attribute__textAlignVert}
                                                value={textAlignVert}
                                                onChange={value => setAttributes({textAlignVert: value})}

                                            />
                                            }

                                            {(thisConfig.attribute__textSize.active) &&
                                            <ButtonGroupSizes
                                                {...thisConfig.attribute__textSize}
                                                value={textSize}
                                                onChange={value => setAttributes({textSize: value})}

                                            />
                                            }

                                            {(thisConfig.attribute__textDropCap.active) &&
                                            <ToggleControl
                                                label={thisConfig.attribute__textDropCap.label}
                                                help={thisConfig.attribute__textDropCap.help}
                                                checked={textDropCap === true}
                                                value={textDropCap}
                                                onChange={value => setAttributes({textDropCap: value})}
                                            />
                                            }


                                        </div>
                                    )

                                } else {
                                    return (

                                        <div className={defaults.tabPanelInnerWrapperClassname}>

                                            {thisConfig.attribute__contentAniName.active &&
                                            <SelectAnimateCSS
                                                {...thisConfig.attribute__contentAniName}
                                                value={contentAniName}
                                                onChange={value => setAttributes({contentAniName: value})}
                                            />
                                            }

                                            {false && thisConfig.attribute__contentAniName.active &&
                                            <SelectAOSAimations
                                                {...thisConfig.attribute__contentAniName}
                                                value={contentAniName}
                                                onChange={value => setAttributes({contentAniName: value})}
                                            />
                                            }


                                            {(!thisConfig.attribute__contentAniDuration.removeOthers.includes(contentAniName)) &&
                                            <Fragment>

                                                {(thisConfig.attribute__contentAniDuration.active) &&
                                                <ButtonGroupTimeDuration
                                                    {...thisConfig.attribute__contentAniDuration}
                                                    value={contentAniDuration}
                                                    onChange={value => setAttributes({contentAniDuration: value})}
                                                />
                                                }

                                                {(!thisConfig.attribute__contentAniDuration.removeOthers.includes(contentAniDuration)) &&
                                                <Fragment>

                                                    {(thisConfig.attribute__contentAniDelay.active) &&
                                                    <ButtonGroupTimeDelay
                                                        {...thisConfig.attribute__contentAniDelay}
                                                        value={contentAniDelay}
                                                        onChange={value => setAttributes({contentAniDelay: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__contentAniIterations.active) &&
                                                    <RangePickerAnimateIterations
                                                        {...thisConfig.attribute__contentAniIterations}
                                                        value={contentAniIterations}
                                                        onChange={value => setAttributes({contentAniIterations: value})}

                                                    />
                                                    }

                                                    {(false && thisConfig.attribute__contentAniEasing.active) &&
                                                    <SelectAOSEasing
                                                        {...thisConfig.attribute__contentAniEasing}
                                                        value={contentAniEasing}
                                                        onChange={value => setAttributes({contentAniEasing: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__contentAniEleAnchor.active) &&
                                                    <RadioPositionVertical
                                                        {...thisConfig.attribute__contentAniEleAnchor}
                                                        value={contentAniEleAnchor}
                                                        onChange={value => setAttributes({contentAniEleAnchor: value})}

                                                    />
                                                    }

                                                    {(thisConfig.attribute__contentAniWinAnchor.active) &&
                                                    <RadioPositionVertical
                                                        {...thisConfig.attribute__contentAniWinAnchor}
                                                        value={contentAniWinAnchor}
                                                        onChange={value => setAttributes({contentAniWinAnchor: value})}

                                                    />
                                                    }


                                                    {(thisConfig.attribute__contentAniOffset.active) &&
                                                    <ButtonGroupAnimateOffset
                                                        {...thisConfig.attribute__contentAniOffset}
                                                        value={contentAniOffset}
                                                        onChange={value => setAttributes({contentAniOffset: value})}

                                                    />
                                                    }

                                                </Fragment>
                                                }
                                            </Fragment>
                                            }

                                        </div>
                                    )
                                }
                            }
                        }
                    </TabPanel>

                </PanelBody>

            </Fragment>
        );
    }
}

export default PanelBodyRichTextSingleCustomize;
