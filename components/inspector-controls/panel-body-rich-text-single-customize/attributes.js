const attributes = {

    textTagName: {
        type: "string"
    },
    textListStyleType: {
        type: "string"
    },
    textDropCap: {
        type: "boolean"
    },
    textSize: {
        type: "string"
    },
    textAlign: {
        type: "string"
    },
    textAlignVert: {
        type: "string"
    },

    // --
    contentAniName: {
        type: "string"
    },
    contentAniDuration: {
        type: "string"
    },
    contentAniDelay: {
        type: "string"
    },
    contentAniIterations: {
        type: "string"
    },
    contentAniEasing: {
        type: "string"
    },
    contentAniEleAnchor:{
        type: "string"
    },
    contentAniWinAnchor:{
        type: "string"
    },
    contentAniOffset: {
        type: "string"
    }

};

export default attributes;