import attrs from "./attributes";

function config(args = {}){

    if ( typeof args === 'undefined' || typeof args !== 'object'){
        args = {};
    }

    let obj = {};

    for (var attr in attrs) {
        obj['attribute__' + attr] = {};
        obj['attribute__' + attr].active = true;
    }

    obj.panelBodyTitle = args.panelBodyTitle || 'Customize';
    obj.initialOpen = args.initialOpen || false;

    obj.attribute__textDropCap.label = 'Drop Cap';
    // obj.attribute__textDropCap.ariaLabel = obj.attribute__textDropCap.label;
    obj.attribute__textDropCap.help = 'Toggle to show a large initial letter.';

    obj.attribute__textSize.label = 'Text Size';
    obj.attribute__textSize.ariaLabel = obj.attribute__textSize.label;

    obj.attribute__textAlignVert.label = 'Text Align Vertical';
    obj.attribute__textAlignVert.ariaLabel = obj.attribute__textAlignVert.label;

    obj.attribute__contentAniName.label = 'Content Animate';
    obj.attribute__contentAniName.ariaLabel = obj.attribute__contentAniName.label;

    obj.attribute__contentAniDuration.label = 'Content Duration';
    obj.attribute__contentAniDuration.ariaLabel = obj.attribute__contentAniDuration.label;

    obj.attribute__contentAniDelay.label = 'Content Delay';
    obj.attribute__contentAniDelay.ariaLabel = obj.attribute__contentAniDelay.label;

    obj.attribute__contentAniEasing.label = 'Content Easing';
    obj.attribute__contentAniEasing.ariaLabel = obj.attribute__contentAniEasing.label;

    obj.attribute__contentAniOffset.label = 'Content Offset';
    obj.attribute__contentAniOffset.ariaLabel = obj.attribute__contentAniOffset.label;

    obj.attribute__contentAniEleAnchor.label = 'Content Edge';
    obj.attribute__contentAniEleAnchor.ariaLabel = obj.attribute__contentAniEleAnchor.label;

    obj.attribute__contentAniWinAnchor.label = 'Content Edge Intersects Window';
    obj.attribute__contentAniWinAnchor.ariaLabel = obj.attribute__contentAniWinAnchor.label;


    // -- merge
    for (attr in attrs) {

        // only merge is args.* === object
        if ( typeof args['attribute__' + attr] === 'object' ){
            obj['attribute__' + attr] = {...obj['attribute__' + attr], ...args['attribute__' + attr]};
        }
    }


    // -- validation


    // attribute__textSize
    if  ( typeof obj.attribute__textSize.exclude === 'undefined' || ! Array.isArray(obj.attribute__textSize.exclude) ){
        obj.attribute__textSize.exclude = ['xxs', 'xs', 'xl', 'xxl'];
    }

    // attribute__contentAniName
    if  ( typeof obj.attribute__contentAniName.removeOthers === 'undefined'
    || ! Array.isArray(obj.attribute__contentAniName.removeOthers) ){
        obj.attribute__contentAniName.removeOthers = ['none'];
    }

    // attribute__contentDuration
    if  ( typeof obj.attribute__contentAniDuration.removeOthers === 'undefined'
        || ! Array.isArray(obj.attribute__contentAniDuration.removeOthers) ){
        obj.attribute__contentAniDuration.removeOthers = ['none'];
    }


    return obj;

}

export default config;

