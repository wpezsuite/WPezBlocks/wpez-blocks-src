function config(args = {}, rtsType) {

    if (typeof args === 'undefined' || typeof args !== 'object') {
        args = {};
    }

    let formattingControls = ['bold', 'italic', 'link'];
    let textListStyleTypeUL = ['none', 'disc', 'circle', 'square', 'custom'];

    // component defaults
    let obj = {};
    switch (rtsType) {

        case 'head':
            obj.placeholder = 'Write heading...';
            obj.ariaLabel = 'Empty block; Write heading';
            break;

        case 'list':
            obj.placeholder = 'Write list...';
            obj.ariaLabel = 'Empty block; Start writing a list';
            break;

        case 'para':
        default:
            obj.placeholder = 'Start writing a paragraph...';
            obj.ariaLabel = 'Empty block; Start writing a paragraph...';
            break;

    }

    obj.active__AttributeFeatureSet = true;

    obj.formattingControls = formattingControls;
    obj.keepPlaceholderOnFocus = true;
    obj.active__AttributeFeatureSet = true;
    obj.active__PanelBodyWrapper = true;
    obj.active__PanelBodyRichTextSingleCustomize = true;
    obj.active__PanelBodyColorCombos = true;

    obj.textListStyleTypeUL = textListStyleTypeUL;

    // "merge"
    obj = {...obj, ...args};

    // validation
    if (!Array.isArray(obj.formattingControls)) {
        obj.formattingControls = formattingControls;
    }

    // validation
    if (!Array.isArray(obj.textListStyleTypeUL)) {
        obj.textListStyleTypeUL = textListStyleTypeUL;
    }


    return obj;
}

export default config;

