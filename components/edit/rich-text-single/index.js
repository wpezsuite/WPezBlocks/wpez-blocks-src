/**
 * External dependencies
 */
import wpezUtils from '../../../lib/js/wpez-utils';

import config from './config';

import RadioWrapperType from "../../attributes/radio-wrapper-type";
import ButtonGroupFeatureSet from "../../attributes/button-group-feature-set";

import PanelBodyWrapper from '../../inspector-controls/panel-body-wrapper';
import PanelBodyRichTextSingleCustomize from '../../inspector-controls/panel-body-rich-text-single-customize';
import PanelBodyColorCombos from '../../inspector-controls/panel-body-color-combos';

/**
 * WordPress dependencies
 */
const {__} = wp.i18n;
const {Component, Fragment} = wp.element;
const {RichText} = wp.editor;
const {InspectorControls} = wp.blockEditor;


/**
 * Block edit function
 */
class RichTextSingle extends Component {

    constructor(props) {
        super(...arguments);
    }


    render() {

        const {
            rtsType = 'para',
            blockName,
            isSelected,
            wpezBC,
            attributes,
            className,
            setAttributes,
        } = this.props;

        const thisBC = wpezBC.get('edit__' + this.constructor.name);
        const thisConfig = config(thisBC, rtsType);

        const {

            featureSet = '01',
            textTagName,
            textListStyleType,
            contentRichText

        } = attributes;

        //const slug = 'wpez-blocks';
        const classes = wpezUtils.textClassnames(blockName, className, attributes);

        let thisTagName = '';
        let thisMultiline = false;
        let thisClassname = false;
        let slug = 'wpez-blocks-rich-text-single';
        let thisWrapperClassName = slug + '-' + rtsType;
        switch (rtsType) {

            case 'head':
                thisTagName = textTagName;
                thisClassname ='can-have-drop-cap';
                break;

            case 'list':
                // TODO - move this array to the config
                thisTagName = (thisConfig.textListStyleTypeUL.includes(textListStyleType)) ? 'ul' : 'ol';
                thisMultiline = 'li';
                thisWrapperClassName = thisWrapperClassName + '-' + thisTagName;
                thisClassname = thisWrapperClassName + '-' + textListStyleType;
                break;

            case 'para':
            default:
                thisTagName = 'p';
                thisClassname ='can-have-drop-cap';
                break;
        }

        return (

            <div className={classes}>

                <InspectorControls>

                    {(isSelected && thisConfig.active__AttributeFeatureSet) &&
                    <ButtonGroupFeatureSet
                        {...thisConfig.attribute__featureSet}
                        value={featureSet}
                        onChange={value => setAttributes({featureSet: value})}
                    />
                    }

                    {(isSelected && thisConfig.active__PanelBodyWrapper) &&
                    <PanelBodyWrapper
                        {...this.props}
                    />
                    }

                    {(isSelected && thisConfig.active__PanelBodyRichTextSingleCustomize) &&
                    <PanelBodyRichTextSingleCustomize
                        {...this.props}
                    />
                    }

                    {(isSelected && thisConfig.active__PanelBodyColorCombos) &&
                    <PanelBodyColorCombos
                        {...this.props}
                    />
                    }

                </InspectorControls>

                <div className="wpez-blocks-block">
                    <div className="wpez-blocks-box">
                        <div className="wpez-blocks-content">
                                <RichText
                                    // inlineToolbar={false}
                                    formattingControls={thisConfig.formattingControls}
                                    tagName={thisTagName}
                                    multiline={thisMultiline}
                                    placeholder={thisConfig.placeholder}
                                    value={contentRichText}
                                    onChange={(value) => setAttributes({contentRichText: value})}
                                    keepPlaceholderOnFocus={thisConfig.keepPlaceholderOnFocus}
                                    unstableOnSplit={() => false}
                                    className={thisClassname}
                                    wrapperClassName={thisWrapperClassName}
                                    aria-label={thisConfig.ariaLabel}
                                />
                        </div>
                    </div>
                </div>
            </div>
        );


    }
}

export default RichTextSingle;