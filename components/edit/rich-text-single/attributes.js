const attributes = {

    featureSet:{
        type: 'string'
    },

    contentRichText: {
        type: 'string'
    }
};

export default attributes;