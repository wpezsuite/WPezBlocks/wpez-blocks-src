
const defaults = {};

defaults.panelBodyClassName = 'wpez-blocks-panel-body';
defaults.tabPanelClassname = 'wpez-blocks-tab-panel';
defaults.tabPanelInnerWrapperClassname = 'wpez-blocks-tab-panel-inner-wrapper';
defaults.labelResetWrapperClassName = 'wpez-blocks-label-reset__wrapper';
defaults.resetClassName = 'components-button__reset';
defaults.tabsWrapper = [
    {
        name: 'setup',
        title: 'Setup',
        className: 'tab-zero',
        disabled: false
    },
    {
        name: 'animate',
        title: "Animate",
        className: 'tab-one',
        disabled: false,
    },
];

defaults.tabsContent = [
    {
        name: 'content',
        title: 'Content',
        className: 'tab-zero',
        disabled: false
    },
    {
        name: 'animate',
        title: "Animate",
        className: 'tab-one',
        disabled: false,
    },
];

export default defaults;
