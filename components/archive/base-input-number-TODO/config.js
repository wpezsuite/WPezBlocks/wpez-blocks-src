import icons from '../../../lib/icons/group/group-block-width';

const { __ } = wp.i18n;

const config = {};

config.name = 'base-input-number-TODO';
config.label =  'Repeat';
config.labelAria =  'Repeat';

config.exclude = [];
config.tooltipPosition =  'top center';

config.buttons = [
    {
        tooltipText: __('Iterations: None', 'wpez-blocks'),
        key: '0',
        buttonIcon: icons.box,
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        tooltipText: __('Iterations: 1', 'wpez-blocks'),
        key: '1',
        buttonIcon: icons.content,
    },
    {
        tooltipText: __('Iterations: 3', 'wpez-blocks'),
        key: '2',
        buttonIcon: icons.wide,
    },
    {
        tooltipText: __('Iterations: 3', 'wpez-blocks'),
        key: '3',
        buttonIcon: icons.full,
    },
    {
        tooltipText: __('Iterations: Custom', 'wpez-blocks'),
        key: 'custom',
        buttonIcon: icons.full,
    },
    {
        tooltipText: __('Iterations: Repeat', 'wpez-blocks'),
        key: 'repeat',
        buttonIcon: 'controls-repeat',
    }
];

  export default config;
