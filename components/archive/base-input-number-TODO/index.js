/**
 * External dependencies
 */
import { isFinite } from 'lodash';
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { compose, withInstanceId, withState } = wp.compose;

/**
 * Internal dependencies
 */
const { BaseControl, IconButton, Button, Dashicon } = wp.components;

var ndx = 0;

function InputNumber( {
                           className,
                           currentInput,
                           label,
                           value,
                           instanceId,
                           onChange,
                           beforeIcon,
                           afterIcon,
                           help,
                           allowReset,
                           initialPosition,
                           min,
                           max,
                           setState,
                           ...props
                       } ) {
    const id = `inspector-range-control-${ instanceId }`;

    const arr = [2,4,6,8];


    let currentInputValue = currentInput === null ? value : currentInput;
    const resetValue = () => {
        resetCurrentInput();
        onChange();
    };
    const resetCurrentInput = () => {
        if ( currentInput !== null ) {
            setState( {
                currentInput: null,
            } );
        }
    };

    const increaseValue = () => {

        if ( ndx < arr.length) {
            ndx++;
            console.log(ndx);
            currentInputValue = arr[ndx];
            onChange(currentInputValue);
        }
    };

    const decreaseValue = () => {
        currentInput--;
        // resetCurrentInput();
     //   onChange();
    };


    const onChangeValue = ( event ) => {
        const newValue = event.target.value;
        // If the input value is invalid temporarily save it to the state,
        // without calling on change.
        if ( ! event.target.checkValidity() ) {
            setState( {
                currentInput: newValue,
            } );
            return;
        }
        // The input is valid, reset the local state property used to temporaly save the value,
        // and call onChange with the new value as a number.
        resetCurrentInput();
        onChange( ( newValue === '' ) ?
            undefined :
            parseFloat( newValue )
        );
    };
    const initialSliderValue = isFinite( currentInputValue ) ?
        currentInputValue :
        initialPosition || '';

    return (
        <BaseControl
            label={ label }
            id={ id }
            help={ help }
            className={ classnames( 'components-range-control', className ) }
        >
            { beforeIcon && <Dashicon icon={ beforeIcon } /> }

            <IconButton onClick={ decreaseValue }
            icon='arrow-left-alt2'>
                { __( 'Prev' ) }
            </IconButton>
            <input
                className="components-range-control__number"
                type="number"
                onChange={ onChangeValue }
                aria-label={ label }
                value={ currentInputValue }
                min={ min }
                max={ max }
                onBlur={ resetCurrentInput }
                { ...props }
            />
            <IconButton onClick={ increaseValue }
                        tooltip={ __( 'Next' )}
            icon='arrow-right-alt2'>

            </IconButton>

            { allowReset &&
            <Button onClick={ resetValue } disabled={ value === undefined }>
                { __( 'Reset' ) }
            </Button>
            }
        </BaseControl>
    );
}

export default compose( [
    withInstanceId,
    withState( {
        currentInput: null,
    } ),
] )( InputNumber );