import icons from '../../icons/group/group-align-vertical';

const { __, sprintf } = wp.i18n;

const config = {};

config.name = 'button-group-align-vertical';
config.type = 'components';
config.labelAria =  'Align Vertical';
config.fieldClass = 'wpez-blocks-button-group';

config.exclude = [];

config.arrMarginRight = ['xs','sm', 'md', 'lg'];
config.marginRight = 'sm';

config.arrPadding = ['sm', 'md', 'lg'];
config.padding = 'md';

config.tooltipPosition =  'top left';

config.buttons = [
    {
        tooltipText: __('Align: Top', 'wpez-blocks'),
        key: 'top',
        buttonIcon: icons.top,
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        tooltipPosition: 'top center',
    },
    {
        tooltipText: __('Align: Center', 'wpez-blocks'),
        key: 'center',
        buttonIcon: icons.center,
        tooltipPosition: 'top center',
    },
    {
        tooltipText: __('Align: Bottom', 'wpez-blocks'),
        key: 'bottom',
        buttonIcon: icons.bottom,
        tooltipPosition: 'bottom center'
    },
];

export default config;
