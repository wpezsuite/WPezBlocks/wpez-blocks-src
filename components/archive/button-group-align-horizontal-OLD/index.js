/**
 * External dependencies
 */
import wpezUtils from '../../lib/js/wpez-utils';
import map from 'lodash/map';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import config from './config'

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const {Component, Fragment} = wp.element;
const {ButtonGroup, IconButton} = wp.components;

/**
 * ButtonGroupSizes Component
 */
class ButtonGroupAlignHorizontal extends Component {

    constructor() {
        super(...arguments);
    }

    render() {
        const {
            label,
            ariaLabel,
            help,
            value,
            exclude,
            classField,
            btnSpacing,
            btnSize,
            onChange,
            buttons,
            btngrpAlignHoriz
        } = this.props;

        const arrButtons = (buttons) ? Array.isArray(buttons) ? buttons : config.buttons : config.buttons;
        const arrExclude = (exclude) ? Array.isArray(exclude) ? exclude : config.exclude : config.exclude;
        const labelAria = (ariaLabel) ? ariaLabel : config.labelAria;
        const fieldClass = (classField) ? classField : '';
        const marginRight = (btnSpacing) ? config.arrMarginRight.includes(btnSpacing) ? btnSpacing : config.marginRight : config.marginRight;
        const padding = (btnSize) ? config.arrPadding.includes(btnSize) ? btnSize : config.padding : config.padding;

        const classesField = wpezUtils.classenames('field', config.type, config.name, [fieldClass, config.fieldClass, 'btn-margin-right-' + marginRight, 'btn-padding-' + padding]);
        const classesLabel = wpezUtils.classenames('label', config.type, config.name);
        const classesHelp = wpezUtils.classenames('help', config.type, config.name);
        const classesItemWrapper = wpezUtils.classenames('item_wrapper', config.type, config.name);

        return (
            <Fragment>
                <div className={classesField}>
                    {label && <span className={classesLabel}>{label}</span>}

                    <ButtonGroup aria-label={__(labelAria)}>
                        {map(arrButtons, (row, i) => {

                            let ariaLabel = (row.ariaLabel) ? row.ariaLabel : (row.tooltipText)  ? row.tooltipText : '';
                            let labelPosition = (row.tooltipPosition) ? row.tooltipPosition : config.tooltipPosition;

                            if (row.key && row.tooltipText && row.buttonIcon) {
                                if (!arrExclude.includes(row.key)) {
                                    return (
                                        <span className={classesItemWrapper}>
								<IconButton
                                    icon={row.buttonIcon}
                                    tooltip={row.tooltipText}
                                    labelPosition={labelPosition}
                                    label={ariaLabel}
                                    isPrimary={row.key === value ? true : false}
                                    aria-pressed={!!btngrpAlignHoriz}
                                    onClick={() => onChange(row.key)}
                                ></IconButton>
						</span>
                                    )
                                }
                            }
                        })}
                    </ButtonGroup>
                    {help && <p className={classesHelp}>{help}</p>}
                </div>
            </Fragment>
        );
    }
}

export default ButtonGroupAlignHorizontal;
