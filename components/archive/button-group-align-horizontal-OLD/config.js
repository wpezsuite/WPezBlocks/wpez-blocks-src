import icons from '../../icons/group/group-align-horizontal';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-block-align-horizontal';
config.type = 'components';
config.labelAria =  'Align Horizontal';
config.fieldClass = 'wpez-blocks-button-group';

config.exclude = ['Xxxs', 'Xxxl'];

config.labelAria =  'Block Width';

config.arrMarginRight = ['xs','sm', 'md', 'lg'];
config.marginRight = 'sm';

config.arrPadding = ['sm', 'md', 'lg'];
config.padding = 'md';

config.tooltipPosition = 'top center';

config.buttons = [
    {
        tooltipText: __('Align: Left', 'wpez-blocks'),
        key: 'left',
        buttonIcon: icons.left
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        tooltipText: __('Align: Center', 'wpez-blocks'),
        key: 'center',
        buttonIcon: icons.center
    },
    {
        tooltipText: __('Align: Right', 'wpez-blocks'),
        key: 'right',
        buttonIcon: icons.right
    },
];

export default config;
