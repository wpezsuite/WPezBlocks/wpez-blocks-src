/**
 * External dependencies
 */
import wpezHelpers from '../../../lib/js/wpez-utils';
import map from 'lodash/map';

/**
 * Internal dependencies
 */
import './styles/editor.scss';

/**
 * WordPress dependencies
 */
const { __, sprintf } = wp.i18n;
const { Component, Fragment } = wp.element;
const { Tooltip, ButtonGroup, Button, ColorIndicator } = wp.components;

/**
 * ButtonGroupColorCombos Component
 */
class ButtonGroupRangeBars extends Component {

	constructor() {
		super( ...arguments );
	}

	render() {
		const {
			exclude,
			label,
			labelAria,
			help,
			combos,
			columns,
			orientation,
			value,
			onChange,
			btngrpCombos
		} = this.props;

		const arrExclude = (exclude) ?  Array.isArray(exclude) ? exclude : [] : [];
		const ariaLabel = (labelAria) ? labelAria : 'Color Combinations';
		const colCnt = (columns) ? [1,'1',2,'2',3,'3',4,'4',5,'5'].includes(columns) ? columns: '3' : '3';
		const btnOrient = (orientation) ? ['portrait', 'landscape' ].includes(orientation) ? orientation: 'landscape' : 'landscape';



		const wpezName = 'button-group-color-combos';
		const wpezType = 'components'

		const classesWrapper = wpezHelpers.classenames('wrapper', wpezType, wpezName, ['columns-' + colCnt, 'orientation-' + btnOrient ])
		const classesLabel = wpezHelpers.classenames('label', wpezType, wpezName)
		const classesHelp = wpezHelpers.classenames('help', wpezType, wpezName)

		const myArgs = [
			{
				tooltipText: 'Slowest',
				key: 'range-bars-0',
			},
			{
				tooltipText: 'Slower',
				key: 'range-bars-1',
			},
			{
				tooltipText: 'Slow',
				key: 'range-bars-2',
		},
		{
			tooltipText: 'Neutral',
			key: 'range-bars-3',
		},
		{
			tooltipText: 'Fast',
			key: 'range-bars-4',
		},
		{
			tooltipText: 'Faster',
			key: 'range-bars-5',
		},
		{
			tooltipText: 'Fastest',
			key: 'range-bars-6',
		}
		];


		return (
			<Fragment>
				<div className={classesWrapper}>
				{ label && <span className={ classesLabel }>{ label }</span> }
				<ButtonGroup aria-label={ __( ariaLabel ) }>
				{map(myArgs, (row, i) => {
					if ( row.tooltipText && row.key && row.combo ){
					if ( ! arrExclude.includes(row.key) && Array.isArray(row.combo)){
						return (
							<span class="wpez-btn-wrapper">
						<Tooltip text={ row.tooltipText }>
						<Button
						key={ row.key }
						isLarge
						isPrimary={row.key === value ? true : false }
						aria-pressed={ !! btngrpCombos }
						onClick={ () => onChange( row.key ) }
						>
						{map(row.combo, (color, i) =>{
							return (
							<ColorIndicator
							colorValue={color}>
							</ColorIndicator>)
						}
						)}
						{ row.buttonText &&
							<span class="wpez-btn-text">
							{row.buttonText}
							</span>
						}
						</Button>
						</Tooltip>
						</span>
						)
					}
				}
				})}
				</ButtonGroup>
					{ help && <p className={ classesHelp }>{ help }</p> }
				</div>
			</Fragment>
		);
	}
}

export default ButtonGroupColorCombos;
