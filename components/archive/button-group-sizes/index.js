/**
 * External dependencies
 */
import wpezUtils from '../../lib/js/wpez-utils';
import map from 'lodash/map';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import config  from'./config'

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;
const { Tooltip, ButtonGroup, Button } = wp.components;

/**
 * ButtonGroupSizes Component
 */
class ButtonGroupSizes extends Component {

	constructor() {
		super( ...arguments );
	}

	render() {
		const {
			label,
			ariaLabel,
			help,
			exclude,
			classField,
			btnSpacing,
			btnSize,
			buttons,

			onChange,
			value
		} = this.props;

		const arrButtons = (buttons) ?  Array.isArray(buttons) ? buttons : config.buttons : config.buttons;
		const arrExclude = (exclude) ?  Array.isArray(exclude) ? exclude : config.exclude : config.exclude;
		const labelAria = (ariaLabel) ? ariaLabel : config.labelAria;
		const fieldClass = (classField) ? classField : '';
		const marginRight = (btnSpacing) ? config.arrMarginRight.includes(btnSpacing) ? btnSpacing: config.marginRight : config.marginRight;
		const padding = (btnSize) ? config.arrPadding.includes(btnSize) ? btnSize: config.padding : config.padding;

		const classesField = wpezUtils.classenames('field', config.type, config.name, [fieldClass, config.fieldClass, 'btn-margin-right-' + marginRight, 'btn-padding-' + padding]);
		const classesLabel = wpezUtils.classenames('label', config.type, config.name);
		const classesHelp = wpezUtils.classenames('help', config.type, config.name);
		const classesItemWrapper = wpezUtils.classenames('item_wrapper', config.type, config.name);


		return (
			<Fragment>
				<div className={classesField}>
				{ label && <span className={ classesLabel }>{ label }</span> }
				-- TODO reset --

				<ButtonGroup aria-label={ __( labelAria) }>
					{map(arrButtons, (row, i) => {
						if ( row.key && row.tooltipText && row.buttonText){
						if ( ! arrExclude.includes(row.key)){
							return (
								<Tooltip text={ row.tooltipText }>
								<Button
								key={ row.key }
								isLarge
								isTertiary
								isPrimary={row.key === value ? true : false }
								aria-pressed={row.key === value ? true : false }
								onClick={ () => onChange( row.key ) }
								>{row.buttonText}</Button>
								</Tooltip>
							)
						}
					}})}
				</ButtonGroup>
				{ help && <p className={ classesHelp }>{ help }</p> }
				</div>
			</Fragment>
		);
	}
}

export default ButtonGroupSizes;
