const { __, sprintf } = wp.i18n;

const config = {};

config.name = 'button-group-sizes';
config.type = 'components';
config.labelAria =  'Select Size';
config.fieldClass = 'wpez-blocks-button-group';

config.exclude = ['xxs', 'xxl'];

config.arrMarginRight = ['xs','sm', 'md', 'lg']
config.marginRight = 'sm'

config.arrPadding = ['sm', 'md', 'lg']
config.padding = 'md'

config.buttons = [
    {
        tooltipText: __('XX-Small', 'wpez-blocks'),
        key: 'xxs',
        buttonText: __('XXS', 'wpez-blocks'),
    },
    {
        tooltipText: __('X-Small', 'wpez-blocks'),
        key: 'xs',
        buttonText: __('XS', 'wpez-blocks'),
    },
    {
        tooltipText: __('Small', 'wpez-blocks'),
        key: 'sm',
        buttonText:  __('S', 'wpez-blocks'),
    },
    {
        tooltipText: __('Medium', 'wpez-blocks'),
        key: 'md',
        buttonText: __('M', 'wpez-blocks'),
    },
    {
        tooltipText: __('Large', 'wpez-blocks'),
        key: 'lg',
        buttonText: __('L', 'wpez-blocks'),
    },
    {
        tooltipText: __('X-Large', 'wpez-blocks'),
        key: 'xl',
        buttonText: __('XL', 'wpez-blocks'),
    },
    {
        tooltipText: __('XX-Large', 'wpez-blocks'),
        key: 'xxl',
        buttonText: __('XXL', 'wpez-blocks'),
    }
];

  export default config;
