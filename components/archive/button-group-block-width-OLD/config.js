import icons from '../../icons/group/group-block-width';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-block-width';
config.type = 'components';
config.labelAria =  'Block Width';
config.fieldClass = 'wpez-blocks-button-group';

config.exclude = ['Xxxs', 'Xxxl'];

config.arrMarginRight = ['xs','sm', 'md', 'lg'];
config.marginRight = 'sm';

config.arrPadding = ['sm', 'md', 'lg'];
config.padding = 'md';

config.tooltipPosition =  'top center';

config.buttons = [
    {
        tooltipText: __('Width: Inner Box', 'wpez-blocks'),
        key: 'box',
        buttonIcon: icons.box,
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        tooltipText: __('Width: Content', 'wpez-blocks'),
        key: 'content',
        buttonIcon: icons.content,
    },
    {
        tooltipText: __('Width: Wide', 'wpez-blocks'),
        key: 'wide',
        buttonIcon: icons.wide,
    },
    {
        tooltipText: __('Width: Full', 'wpez-blocks'),
        key: 'full',
        buttonIcon: icons.full,
    }
];

  export default config;
