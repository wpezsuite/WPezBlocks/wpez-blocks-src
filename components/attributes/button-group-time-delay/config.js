//import icons from '../../lib/icons/group/group-block-width';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-time-delay';
config.label =  'Animation Delay';
config.labelAria =  'Animation Delay';

config.exclude = [];
config.tooltipPosition =  'top center';

config.buttons = [
    {
        buttonText: __('None', 'wpez-blocks'),
        tooltipText: __('Delay: None', 'wpez-blocks'),
        key: 'none',
        value: 'none',
        icon: 'dismiss',
        className: 'wpez-blocks-btn-none'
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        buttonText: __('Shortest', 'wpez-blocks'),
        tooltipText: __('Delay: Shortest', 'wpez-blocks'),
        key: 'shortest',
        value: 'shortest',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-shortest'
    },
    {
        buttonText: __('Shorter', 'wpez-blocks'),
        tooltipText: __('Delay: Shorter', 'wpez-blocks'),
        key: 'shorter',
        value: 'shorter',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-shorter'
    },
    {
        buttonText: __('Delay: Short', 'wpez-blocks'),
        tooltipText: __('Delay: Short', 'wpez-blocks'),
        key: 'short',
        value: 'short',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-short'
    },
    {
        buttonText: __('Delay: Long', 'wpez-blocks'),
        tooltipText: __('Delay: Long', 'wpez-blocks'),
        key: 'long',
        value: 'long',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-long'
    },
    {
        buttonText: __('Delay: Longer', 'wpez-blocks'),
        tooltipText: __('Delay: Longer', 'wpez-blocks'),
        key: 'longer',
        value: 'longer',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-longer'
    },
    {
        buttonText: __('Delay: Longest', 'wpez-blocks'),
        tooltipText: __('Delay: Longest', 'wpez-blocks'),
        key: 'longest',
        value: 'longest',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-longest'
    }

];

config.defaultKey = 'none';
config.resetTooltip = 'Reset to Delay: None';
config.exclude = [ 'longest'];
config.disabled = [];


  export default config;
