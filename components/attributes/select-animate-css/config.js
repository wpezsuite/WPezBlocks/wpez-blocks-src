
// const { __ } = wp.i18n;

const config = {};

config.name = 'select-animate-css';
config.label = 'Animate';
config.ariaLabel = 'Animate';

config.options= [
    {label: '', value: 'none', type: 'none'},

    {optgroup: true, label: 'Attention Seekers', value: 'attention-seekers', type: 'oth'},
    {label: 'bounce', value: 'bounce', type: 'oth'},
    {label: 'flash', value: 'flash', type: 'oth'},
    {label: 'pulse', value: 'pulse', type: 'oth'},
    {label: 'rubberBand', value: 'rubberBand', type: 'oth'},
    {label: 'shake', value: 'shake', type: 'oth'},
    {label: 'swing', value: 'swing', type: 'oth'},
    {label: 'tada', value: 'tada', type: 'oth'},
    {label: 'wobble', value: 'wobble', type: 'oth'},
    {label: 'jello', value: 'jello', type: 'oth'},
    {label: 'heartBeat', value: 'heartBeat', type: 'oth'},
    {label: 'flip', value: 'flip', type: 'oth'},


    {optgroup: true, label: 'Enter - Bouncing', value: 'bouncing-entrances', type: 'enter'},
    {label: 'bounceIn', value: 'bounceIn', type: 'enter'},
    {label: 'bounceInDown', value: 'bounceInDown', type: 'enter'},
    {label: 'bounceInLeft', value: 'bounceInLeft', type: 'enter'},
    {label: 'bounceInRight', value: 'bounceInRight', type: 'enter'},
    {label: 'bounceInUp', value: 'bounceInUp', type: 'enter'},

    {optgroup: true, label: 'Enter - Fading', value: 'fading-entrances', type: 'enter'},
    {label: 'fadeIn', value: 'fadeIn', type: 'enter'},
    {label: 'fadeInDown', value: 'fadeInDown', type: 'enter'},
    {label: 'fadeInDownBig', value: 'fadeInDownBig', type: 'enter'},
    {label: 'fadeInLeft', value: 'fadeInLeft', type: 'enter'},
    {label: 'fadeInLeftBig', value: 'fadeInLeftBig', type: 'enter'},
    {label: 'fadeInRight', value: 'fadeInRight', type: 'enter'},
    {label: 'fadeInRightBig', value: 'fadeInRightBig', type: 'enter'},
    {label: 'fadeInUp', value: 'fadeInUp', type: 'enter'},
    {label: 'fadeInUpBig', value: 'fadeInUpBig', type: 'enter'},
    {label: 'jackInTheBox', value: 'jackInTheBox', type: 'enter'},

    {optgroup: true, label: 'Enter - Flippers', value: 'flippers-entrances', type: 'enter'},
    {label: 'flipInX', value: 'flipInX', type: 'enter'},
    {label: 'flipInY', value: 'flipInY', type: 'enter'},
    {label: 'rollIn', value: 'rollIn', type: 'enter'},

    {optgroup: true, label: 'Enter - Rotating', value: 'rotating-entrances', type: 'enter'},
    {label: 'rotateIn', value: 'rotateIn', type: 'enter'},
    {label: 'rotateInDownLeft', value: 'rotateInDownLeft', type: 'enter'},
    {label: 'rotateInDownRight', value: 'rotateInDownRight', type: 'enter'},
    {label: 'rotateInUpLeft', value: 'rotateInUpLeft', type: 'enter'},
    {label: 'rotateInUpRight', value: 'rotateInUpRight', type: 'enter'},

    {optgroup: true, label: 'Enter - Sliding', value: 'sliding-entrances', type: 'enter'},
    {label: 'slideInUp', value: 'slideInUp', type: 'enter'},
    {label: 'slideInDown', value: 'slideInDown', type: 'enter'},
    {label: 'slideInLeft', value: 'slideInLeft', type: 'enter'},
    {label: 'slideInRight', value: 'slideInRight', type: 'enter'},
    {label: 'lightSpeedIn', value: 'lightSpeedIn', type: 'enter'},

    {optgroup: true, label: 'Enter - Zoom', value: 'zoom-entrances', type: 'enter'},
    {label: 'zoomIn', value: 'zoomIn', type: 'enter'},
    {label: 'zoomInDown', value: 'zoomInDown', type: 'enter'},
    {label: 'zoomInLeft', value: 'zoomInLeft', type: 'enter'},
    {label: 'zoomInRight', value: 'zoomInRight', type: 'enter'},
    {label: 'zoomInUp', value: 'zoomInUp', type: 'enter'},

    // --

    {optgroup: true, label: 'Exit - Bouncing', value: 'bouncing-exits', type: 'exit'},
    {label: 'bounceOut', value: 'bounceOut', type: 'exit'},
    {label: 'bounceOutDown', value: 'bounceOutDown', type: 'exit'},
    {label: 'bounceOutLeft', value: 'bounceOutLeft', type: 'exit'},
    {label: 'bounceOutRight', value: 'bounceOutRight', type: 'exit'},
    {label: 'bounceOutUp', value: 'bounceOutUp', type: 'exit'},

    {optgroup: true, label: 'Exit - Fading', value: 'fading-exits', type: 'exit'},
    {label: 'fadeOut', value: 'fadeOut', type: 'exit'},
    {label: 'fadeOutDown', value: 'fadeOutDown', type: 'exit'},
    {label: 'fadeOutDownBig', value: 'fadeOutDownBig', type: 'exit'},
    {label: 'fadeOutLeft', value: 'fadeOutLeft', type: 'exit'},
    {label: 'fadeOutLeftBig', value: 'fadeOutLeftBig', type: 'exit'},
    {label: 'fadeOutRight', value: 'fadeOutRight', type: 'exit'},
    {label: 'fadeOutRightBig', value: 'fadeOutRightBig', type: 'exit'},
    {label: 'fadeOutUp', value: 'fadeOutUp', type: 'exit'},
    {label: 'fadeOutUpBig', value: 'fadeOutUpBig', type: 'exit'},

    {optgroup: true, label: 'Exit - Flippers', value: 'flippers', type: 'exit'},
    {label: 'flipOutX', value: 'flipOutX', type: 'exit'},
    {label: 'flipOutY', value: 'flipOutY', type: 'exit'},
    {label: 'rollOut', value: 'rollOut', type: 'exit'},

//    {optgroup: true, label: 'Lightspeed', value: 'lightspeed'},

    {optgroup: true, label: 'Exit - Rotating', value: 'rotating-exits', type: 'exit'},
    {label: 'rotateOut', value: 'rotateOut', type: 'exit'},
    {label: 'rotateOutDownLeft', value: 'rotateOutDownLeft', type: 'exit'},
    {label: 'rotateOutDownRight', value: 'rotateOutDownRight', type: 'exit'},
    {label: 'rotateOutUpLeft', value: 'rotateOutUpLeft', type: 'exit'},
    {label: 'rotateOutUpRight', value: 'rotateOutUpRight', type: 'exit'},
    {label: 'hinge', value: 'hinge', type: 'exit'},

    {optgroup: true, label: 'Exit - Sliding', value: 'sliding-exits', type: 'exit'},
    {label: 'slideOutUp', value: 'slideOutUp', type: 'exit'},
    {label: 'slideOutDown', value: 'slideOutDown', type: 'exit'},
    {label: 'slideOutLeft', value: 'slideOutLeft', type: 'exit'},
    {label: 'slideOutRight', value: 'slideOutRight', type: 'exit'},
    {label: 'lightSpeedOut', value: 'lightSpeedOut', type: 'exit'},

    {optgroup: true, label: 'Exit - Zoom', value: 'zoom-exits', type: 'exit'},
    {label: 'zoomOut', value: 'zoomOut', type: 'exit'},
    {label: 'zoomOutDown', value: 'zoomOutDown', type: 'exit'},
    {label: 'zoomOutLeft', value: 'zoomOutLeft', type: 'exit'},
    {label: 'zoomOutRight', value: 'zoomOutRight', type: 'exit'},
    {label: 'zoomOutUp', value: 'zoomOutUp', type: 'exit'},

];

// config.defaultKey =
config.defaultOptionIndex = 0;
config.defaultValue = 'none';

config.typeInclude = ['none', 'oth', 'enter', 'exit'];
config.optgroupInclude = [];
config.optgroupExclude = [];
config.optgroupDisabled = [];

config.optionInclude = [];
config.optionExclude = [];
config.optionDisabled = [];

// config.help =
config.classField = 'wpez-blocks-select animate-css';
config.resetActive = true;
config.resetButtonText = 'Reset';
config.resetTooltip = false;

export default config;