/**
 * External dependencies
 */
/*
import wpezUtils from '../../lib/js/wpez-utils';
import map from 'lodash/map';
*/

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import config  from'./config'
import ButtonGroupBasicButtons from "../base/base-button-group-basic-buttons";

/**
 * WordPress dependencies
 */
const { Component, Fragment } = wp.element;

/**
 * ButtonGroupSizes Component
 */
class ButtonGroupFeatureSet extends Component {

	constructor(props) {
		super( ...arguments );
	}

	render() {

		return (
			<Fragment>
				<ButtonGroupBasicButtons
					config={ config }
					{...this.props}
				/>
			</Fragment>
		);
	}
}

export default ButtonGroupFeatureSet;
