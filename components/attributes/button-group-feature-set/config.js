const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-feature-set';
config.label = 'Feature Set';
config.ariaLabel = 'Feature Set';


//config.buttons = [
    // buttonText: ,
    // ariaLabel  << if empty will default to tootipText
    // tooltipText: ,  << optional
    // tooltipPosition: , << optional
    // key: ,   // required
    // value: , // required
    // default: bool,  << only specify once
    // disabled: bool, << optional
    // className:
//];

config.buttons = [
    {
        tooltipText: __('Basic', 'wpez-blocks'),
        key: '01',
        value: '01',
        buttonText: __('Basic', 'wpez-blocks'),
    },
    {
        tooltipText: __('Extra', 'wpez-blocks'),
        key: '50',
        value: '50',
        buttonText: __('Extra', 'wpez-blocks'),
    },
    {
        tooltipText: __('Advanced', 'wpez-blocks'),
        key: '99',
        value: '99',
        buttonText:  __('Advanced', 'wpez-blocks'),
    }
];

config.defaultButtonIndex = 0;
config.defaultKey = '01';
config.resetActive = false;
config.resetTooltip = 'Reset to Basic  xxxxx';

config.rowClassName = 'button-key-%s';
config.exclude = [];
config.disabled = [];
// config.help =

export default config;
