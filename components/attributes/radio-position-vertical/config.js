
// const { __ } = wp.i18n;

const config = {};

config.name = 'radio-position-vertical';
config.label = 'Position Vertical';
config.ariaLabel = 'Position Vertical';

config.options= [
    {label: 'Top', value: 'top', type: 'top'},
    {label: 'Center', value: 'center', type: 'center'},
    {label: 'Bottom', value: 'bottom', type: 'bottom'},
];

// config.defaultKey =
config.defaultOptionIndex = 0;
config.defaultValue = 'center';

config.typeInclude = ['top', 'center', 'bottom'];
config.optgroupInclude = [];
config.optgroupExclude = [];
config.optgroupDisabled = [];

config.optionInclude = [];
config.optionExclude = [];
config.optionDisabled = [];

// config.help =
config.classField = 'wpez-blocks-radio position-vertical';
config.resetActive = true;
config.resetButtonText = 'Reset';
config.resetTooltip = false;

export default config;