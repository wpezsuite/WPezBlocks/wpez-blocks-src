import icons from '../../../lib/icons/group/group-align-horizontal';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-align-horizontal';
config.label =  'Align Horizontal';
config.ariaLabel =  'Align Horizontal';

config.buttons = [
    {
        buttonText:  __('Align Left', 'wpez-blocks'),
        tooltipText: __('Align Left', 'wpez-blocks'),
        key: 'left',
        value: 'left',
        icon: icons.left
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        buttonText:  __('Align Center', 'wpez-blocks'),
        tooltipText: __('Align Center', 'wpez-blocks'),
        key: 'center',
        value: 'center',
        icon: icons.center
    },
    {
        buttonText: __('Align Right', 'wpez-blocks'),
        tooltipText: __('Align Right', 'wpez-blocks'),
        key: 'right',
        value: 'right',
        icon: icons.right
    },
];

config.defaultKey = 'center';
config.resetTooltip = 'Reset to Align Center';
config.exclude = [];
config.disabled = [];

export default config;
