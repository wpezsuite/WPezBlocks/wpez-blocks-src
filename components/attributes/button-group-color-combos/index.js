/**
 * External dependencies
 */
import wpezUtils from '../../../lib/js/wpez-utils';
import map from 'lodash/map';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import globalDefaults from '../../defaults';
import config from './config';

/**
 * WordPress dependencies
 */
const {__} = wp.i18n;
const {Component, Fragment} = wp.element;
const {Tooltip, ButtonGroup, Button, IconButton, ColorIndicator} = wp.components;

/**
 * ButtonGroupColorCombos Component
 */
class ButtonGroupColorCombos extends Component {

    constructor() {
        super(...arguments);
    }

    render() {
        const {
            name,
            label,
            ariaLabel,
            buttons,
            defaultKey,
            help,
            rowClassName,
            exclude,
            disabled,

            classField,
            columns,
            orientation,

            buttonTextActive,
            tooltipActive,
            tooltipPosition,
            resetActive,
            resetButtonText,
            resetTooltip,
            resetTooltipPosition,

            onChange,
            value,
        } = this.props;

        const thisButtons = (buttons) ? Array.isArray(buttons) ? buttons : config.buttons  : config.buttons;

        if (!thisButtons.length) {
            return null;
        }

        const thisName = name || config.name;
        const thisLabel = label || config.label;
        const thisAriaLabel = ariaLabel || config.ariaLabel;
        //  buttons,
        const thisDefaultKey = defaultKey || config.defaultKey;
        const thisRowClassName = rowClassName || config.rowClassName;
        // exclude,
        const thisExclude = (exclude) ? Array.isArray(exclude) ? exclude : config.exclude : config.exclude;
        //  disabled,
        const thisDisabled = (disabled) ? Array.isArray(disabled) ? disabled : config.disabled : config.disabled;
        const thisHelp = help || config.help;
        const thisColumns = (columns) ? [1, '1', 2, '2', 3, '3', 4, '4', 5, '5'].includes(columns) ? columns : config.cols : config.cols;
        const thisOrientation = (orientation) ? ['portrait', 'landscape'].includes(orientation) ? orientation : config.orientation : config.orientation;

        const thisButtonTextActive = buttonTextActive || config.buttonTextActive;
        const thisTooltipActive = tooltipActive || config.tooltipActive;
        const thisTooltipPosition = tooltipPosition || config.toolTipPosition;
        const thisResetActive = resetActive || config.resetActive;
        const thisResetButtonText = resetButtonText || config.resetButtonText;
        const thisResetTooltip = resetTooltip || config.resetTooltip;
        const thisResetTooltipPosition = resetTooltipPosition || config.resetTooltipPosition;

        // --------------------------

        const defaultButton = (thisButtons.find((button) => button.default === true)) ? thisButtons.find((button) => button.default === true) : (thisDefaultKey) ? thisButtons.find((button) => button.key === thisDefaultKey) : thisButtons[config.defaultButtonIndex];
        const currentButton = (value) ? thisButtons.find((button) => button.key === value.toString()) : defaultButton;
        const defaultValue = defaultButton.value;
        // default to the default value
        (value) ? '' : onChange(defaultValue);

        const classesField = wpezUtils.classenames('field', config.type, thisName, [classField, config.classField, 'columns-' + thisColumns, 'orientation-' + thisOrientation]);
        const classesLabel = wpezUtils.classenames('label', config.type, thisName);
        const classesHelp = wpezUtils.classenames('help', config.type, thisName);
        const classesItemWrapper = wpezUtils.classenames('item_wrapper', config.type, thisName);

        return (
            <Fragment>
                <div className={classesField}>
                    {(thisLabel || thisResetActive) &&
                    <div className={globalDefaults.labelResetWrapperClassName}>
                        {thisLabel &&
                        <span className={classesLabel}>{thisLabel}</span>
                        }

                        {(thisResetActive && value !== defaultValue) &&
                        <IconButton
                            tooltip={(thisResetTooltip)}
                            labelPosition={thisResetTooltipPosition}
                            aria-label={(thisResetTooltip) ? thisResetTooltip : thisResetButtonText}
                            className="components-button__reset"
                            type="button"
                            disabled={value === undefined}
                            aria-disabled={value === undefined}
                            onClick={() => onChange(defaultValue)}
                            isSmall
                            isDefault
                        >
                            {thisResetButtonText}
                        </IconButton>
                        }
                    </div>
                    }

                    <ButtonGroup aria-label={__(thisAriaLabel)}>
                        {map(thisButtons, (row, i) => {
                            if (row.tooltipText && row.key && row.combo) {
                                if (!thisExclude.includes(row.key) && Array.isArray(row.combo)) {

                                    // ---

                                    let tooltipText = '';
                                    if (thisTooltipActive === true) {
                                        tooltipText = (row.tooltipText) ? row.tooltipText : false;
                                    }
                                    const rowAriaLabel = (row.ariaLabel) ? row.ariaLabel : (row.tooltipText) ? row.tooltipText : '';
                                    const rowTooltipPosition = (row.tooltipPosition) ? row.tooltipPosition : thisTooltipPosition;
                                    const rowButtonTextActive = (row.buttonTextActive) ? row.buttonTextActive: thisButtonTextActive;
                                    const classNameRow = (row.className) ? row.className : sprintf(thisRowClassName, row.key);

                                    // ---
                                    if (thisTooltipActive === false) {

                                        return (
                                            <span className={[classesItemWrapper + ' ' + classNameRow]}>

						<Button
                            aria-label={rowAriaLabel}
                            key={row.key}
                            isLarge
                            isPrimary={row.key === value ? true : false}
                            aria-pressed={row.key === value ? true : false}
                            disabled={(row.disabled) ? row.disabled : thisDisabled.includes(row.key)}
                            aria-disabled={(row.disabled) ? row.disabled : thisDisabled.includes(row.key)}

                            onClick={() => onChange(row.key)}
                        >
						<span className="wpez-btn-faux">
						{map(row.combo, (color, i) => {
                                return (
                                    <ColorIndicator
                                        colorValue={color}
                                    >
                                    </ColorIndicator>)
                            }
                        )}
                         </span>
                            {rowButtonTextActive && row.buttonText &&
                            <span className="wpez-blocks-btn-text">
							{row.buttonText}
							</span>
                            }

						</Button>
 </span>
                                        )

                                    } else {

                                        return (

                                            <span className={[classesItemWrapper + ' ' + classNameRow]}>
                                                <Tooltip
                                                    text={tooltipText}
                                                    position={rowTooltipPosition}
                                                >
                                                    <Button
                                                        aria-label={rowAriaLabel}
                                                        key={row.key}
                                                        isLarge
                                                        isPrimary={row.key === value ? true : false}
                                                        aria-pressed={row.key === value ? true : false}
                                                        disabled={(row.disabled) ? row.disabled : thisDisabled.includes(row.key)}
                                                        aria-disabled={(row.disabled) ? row.disabled : thisDisabled.includes(row.key)}
                                                        onClick={() => onChange(row.key)}
                                                    >
                                                        <span className="wpez-btn-faux">
                                                            {map(row.combo, (color, i) => {
                                                                return (
                                                                    <ColorIndicator
                                                                        colorValue={color}
                                                                    >
                                                                    </ColorIndicator>)
                                                                }
                                                            )}
                                                        </span>
                                                        {rowButtonTextActive && row.buttonText &&
                                                        <span className="wpez-blocks-btn-text">
                                                            {row.buttonText}
                                                        </span>
                                                        }

                                                    </Button>
                                                </Tooltip>
                                            </span>
                                        )
                                    }
                                }
                            }
                        })}
                    </ButtonGroup>
                    {thisHelp && <p className={classesHelp}>{thisHelp}</p>}
                </div>
            </Fragment>
        );
    }
}

export default ButtonGroupColorCombos;
