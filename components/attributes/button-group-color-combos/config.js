const { __ } = wp.i18n;

const config = {};

config.type = 'components';
config.name = 'button-group-color-combos';
config.label =  'Select Color Combo';
config.ariaLabel =  'Select Color Combo';

// config.buttons = [];

config.rowClassName = 'button-key-%s';
config.exclude = [];
config.disabled = [];
config.classField = 'wpez-blocks-button-group color-combos';

config.cols = '1';
config.orientation = 'landscape';
config.buttonText = false;

config.buttonTextActive = false;
config.tooltipActive = true;
config.tooltipPosition = 'top center';
config.resetActive = true;
config.resetButtonText = 'Reset';
config.resetTooltip = 'Reset to Default';
config.resetTooltipPosition = 'top left';

config.defaultButtonIndex = 0;

export default config;
