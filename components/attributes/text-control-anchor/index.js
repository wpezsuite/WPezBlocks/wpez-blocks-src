/**
 * External dependencies
 */
import wpezUtils from '../../../lib/js/wpez-utils';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import defaults from './defaults'

/**
 * WordPress dependencies
 */
const {Component, Fragment} = wp.element;
const {withInstanceId, withState } = wp.compose;

const ANCHOR_REGEX = /[\s#]/g;

/**
 * ButtonGroupSizes Component
 */
class TextControlAnchor extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        let {
            config,

            instanceId,
            name,
            label,
            ariaLabel,  // TODO
            help,
            classField = '',

            onChange,
            value,
            ...additionalProps
        } = this.props;

        const id = `inspector-text-control-anchor-${ instanceId }`;
        name = name || defaults.name;
        label = label || defaults.label;
        ariaLabel = ariaLabel || defaults.ariaLabel;
        help = help || defaults.help;

        // -------------------------
        const classesField = wpezUtils.classenames('field', defaults.type, name, [classField, defaults.classField]);
        const classesLabel = wpezUtils.classenames('label', defaults.type, name);
        const classesHelp = wpezUtils.classenames('help', defaults.type, name);
        // const classesItemWrapper = wpezUtils.classenames('item_wrapper', defaults.type, name );

        const onChangeValue = ( event ) => {
            let str = event.target.value;
            // don't allow certain leading characters
            str = str.replace( /^[0-9_-]/, '' );
            // GB's "stock" ancgor regex
            str = str.replace( ANCHOR_REGEX, '-' );
            value = str;
            onChange(value);
        };

        return (

            <Fragment>
                <div className={classesField}>
                    {ariaLabel &&
                    <label className="sr-only" htmlFor={id}>{ariaLabel}</label>
                    }
                    { label &&
                    <div className='wpez-blocks-label-reset__wrapper'>
                        <span className={classesLabel}>{label}</span>
                    </div>
                    }
                    <input
                        type={ 'text' }
                        id={id}
                        aria-describedby={!!help ? `${id}__help` : undefined}
                        value={ value }
                        onChange={onChangeValue}
                    />

                    {help &&
                    <p id={ id + '__help'} className={classesHelp}>{help}</p>
                    }
                </div>
            </Fragment>
        )
    }
}

export default withInstanceId(TextControlAnchor);

