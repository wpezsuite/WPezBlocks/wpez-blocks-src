
// const { __ } = wp.i18n;

const defaults = {};

defaults.type = 'components';
defaults.name = 'text-control-anchor';
defaults.label = 'HTML Anchor';
defaults.ariaLabel = 'HTML Anchor';

defaults.help = 'Cannot begin with a number, a - or a _'
defaults.classField = 'wpez-blocks-text';

export default defaults;