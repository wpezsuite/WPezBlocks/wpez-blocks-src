import icons from '../../../lib/icons/group/group-text-align';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-text-align';
config.label =  'Text Align';
config.ariaLabel =  'Text Align';

config.buttons = [
    {
        buttonText:  __('Left-Aligned', 'wpez-blocks'),
        tooltipText: __('Left-Aligned', 'wpez-blocks'),
        key: 'left',
        value: 'left',
        icon: icons.left
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        buttonText:  __('Center-Aligned', 'wpez-blocks'),
        tooltipText: __('Center-Aligned', 'wpez-blocks'),
        key: 'center',
        value: 'center',
        icon: icons.center
    },
    {
        buttonText: __('Right-Aligned', 'wpez-blocks'),
        tooltipText: __('Right-Aligned', 'wpez-blocks'),
        key: 'right',
        value: 'right',
        icon: icons.right
    },
    {
        buttonText: __('Justified', 'wpez-blocks'),
        tooltipText: __('Justified', 'wpez-blocks'),
        key: 'justify',
        value: 'justify',
        icon: icons.justify
    },
];

config.defaultKey = 'left';
config.resetTooltip = 'Reset to Left-Aligned';
config.exclude = [];
config.disabled = [];

export default config;
