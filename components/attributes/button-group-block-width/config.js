import icons from '../../../lib/icons/group/group-block-width';

const {__} = wp.i18n;

const config = {};

config.name = 'button-group-block-width';
config.label = 'Block Width';
config.labelAria = 'Block Width';

config.buttons = [
    {
        buttonText: 'Box',
        tooltipText: __('Width: Inner Box', 'wpez-blocks'),
        key: 'box',
        value: 'box',
        icon: icons.box,
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        buttonText: 'Content',
        tooltipText: __('Width: Content', 'wpez-blocks'),
        key: 'content',
        value: 'content',
        icon: icons.content,
    },
    {
        buttonText: 'Wide',
        tooltipText: __('Width: Wide', 'wpez-blocks'),
        key: 'wide',
        value: 'wide',
        icon: icons.wide,
    },
    {
        buttonText: 'Full',
        tooltipText: __('Width: Full', 'wpez-blocks'),
        key: 'full',
        value: 'full',
        icon: icons.full,
    }
];

config.defaultKey = 'content';
config.resetTooltip = 'Reset to Width: Content';
config.exclude = [];
config.disabled = [];

export default config;
