import icon from '../../../lib/icons/icon/heading';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-heading';
config.label =  'Heading';
config.labelAria =  'Heading';

config.tooltipPosition =  'top center';

config.buttons = [
    {
        tooltipText: __('Heading: H1', 'wpez-blocks'),
        key: 'h1',
        value: 'h1',
        icon: icon,
        dataSubscript: '1',
     //   className: 'wpez-blocks-btn-shortest'
    },
    {
        tooltipText: __('Heading: H2', 'wpez-blocks'),
        key: 'h2',
        value: 'h2',
        icon: icon,
        dataSubscript: '2',
    },
    {
        tooltipText: __('Heading: H3', 'wpez-blocks'),
        key: 'h3',
        value: 'h3',
        icon: icon,
        dataSubscript: '3',
    },
    {
        tooltipText: __('Heading: H4', 'wpez-blocks'),
        key: 'h4',
        value: 'h4',
        icon: icon,
        dataSubscript: '4',
    },
    {
        tooltipText: __('Heading: H5', 'wpez-blocks'),
        key: 'h5',
        value: 'h5',
        icon: icon,
        dataSubscript: '5',
    },
    {
        tooltipText: __('Heading: H6', 'wpez-blocks'),
        key: 'h6',
        value: 'h6',
        icon: icon,
        dataSubscript: '6',
    },

];

config.defaultKey = 'h2';
config.resetTooltip = 'Reset to H2';
config.exclude = [];
config.disabled = [];

  export default config;
