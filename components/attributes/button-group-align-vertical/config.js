import icons from '../../../lib/icons/group/group-align-vertical';

const { __, } = wp.i18n;

const config = {};

config.name = 'button-group-align-vertical';
config.label=  'Align Vertical';
config.labelAria =  'Align Vertical';

config.exclude = [];
config.tooltipPosition =  'top center';

config.buttons = [
    {
        buttonText:__('Align: Top', 'wpez-blocks'),
        tooltipText: __('Align: Top', 'wpez-blocks'),
        key: 'top',
        value: 'top',
        icon: icons.top,
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top center',
    },
    {
        buttonText: __('Align: Center', 'wpez-blocks'),
        tooltipText: __('Align: Center', 'wpez-blocks'),
        key: 'center',
        value: 'center',
        icon: icons.center,
    },
    {
        buttonText: __('Align: Bottom', 'wpez-blocks'),
        tooltipText: __('Align: Bottom', 'wpez-blocks'),
        key: 'bottom',
        value: 'bottom',
        icon: icons.bottom,
    },
];


config.defaultKey = 'center';
config.resetTooltip = 'Reset to Align Center';
config.exclude = [];
config.disabled = [];

export default config;



