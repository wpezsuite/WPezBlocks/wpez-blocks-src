/**
 * External dependencies
 */
import Select from '../base/base-select';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import config from './config'

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const {Component, Fragment} = wp.element;

/**
 * ButtonGroupSizes Component
 */
class SelectAOSAnimations extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        return (
            <Fragment>
                <Select
                    config={ config }
                    {...this.props}
                />
            </Fragment>
        );
    }
}

export default SelectAOSAnimations;