
// const { __ } = wp.i18n;

const config = {};

config.name = 'select-aos-animation';
config.label = 'Animation';
config.ariaLabel = 'Animation';

config.options= [
    {label: '', value: 'none', type: 'none'},

    {optgroup: true, label: 'Enter - Fade', value: 'enter-fade', type: 'fade'},
    {label: 'fade', value: 'fade', type: 'fade'},
    {label: 'fade-up', value: 'fade-up', type: 'fade'},
    {label: 'fade-down', value: 'fade-down', type: 'fade'},
    {label: 'fade-left', value: 'fade-left', type: 'fade'},
    {label: 'fade-up-left', value: 'fade-up-left', type: 'fade'},
    {label: 'fade-down-left', value: 'fade-down-left', type: 'fade'},
    {label: 'fade-right', value: 'fade-right', type: 'fade'},
    {label: 'fade-up-right', value: 'fade-up-right', type: 'fade'},
    {label: 'fade-down-right', value: 'fade-downright', type: 'fade'},

    {optgroup: true, label: 'Enter - Flip', value: 'enter-flip', type: 'flip'},
    {label: 'flip-up', value: 'flip-up', type: 'flip'},
    {label: 'flip-down', value: 'flip-down', type: 'flip'},
    {label: 'flip-left', value: 'flip-left', type: 'flip'},
    {label: 'flip-right', value: 'flip-right', type: 'flip'},

    
    {optgroup: true, label: 'Enter - Slide', value: 'enter-slide', type: 'slide'},
    {label: 'slide-up', value: 'slide-up', type: 'slide'},
    {label: 'slide-down', value: 'slide-down', type: 'slide'},
    {label: 'slide-left', value: 'slide-left', type: 'slide'},
    {label: 'slide-right', value: 'slide-right', type: 'slide'},

    {optgroup: true, label: 'Enter - Zoom', value: 'enter-zoom', type: 'zoom'},
    {label: 'zoom-in', value: 'zoom-in', type: 'zoom'},
    {label: 'zoom-in-up', value: 'zoom-in-up', type: 'zoom'},
    {label: 'zoom-in-down', value: 'zoom-in-down', type: 'zoom'},
    {label: 'zoom-in-left', value: 'zoom-in-left', type: 'zoom'},
    {label: 'zoom-in-right', value: 'zoom-in-right', type: 'zoom'},
    {label: 'zoom-out', value: 'zoom-out', type: 'zoom'},
    {label: 'zoom-out-up', value: 'zoom-out-up', type: 'zoom'},
    {label: 'zoom-out-down', value: 'zoom-out-down', type: 'zoom'},
    {label: 'zoom-out-left', value: 'zoom-out-left', type: 'zoom'},
    {label: 'zoom-out-right', value: 'zoom-out-right', type: 'zoom'},

];

// config.defaultKey =
config.defaultOptionIndex = 0;
config.defaultValue = 'none';

config.typeInclude = ['none', 'fade', 'flip', 'slide', 'zoom'];
config.optgroupInclude = [];
config.optgroupExclude = [];
config.optgroupDisabled = [];

config.optionInclude = [];
config.optionExclude = [];
config.optionDisabled = [];

// config.help =
config.classField = 'wpez-blocks-select aos-animation';
config.resetActive = true;
config.resetButtonText = 'Reset';
config.resetTooltip = false;

export default config;