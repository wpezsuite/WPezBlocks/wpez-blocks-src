
// const { __ } = wp.i18n;

const config = {};

config.name = 'select-list-style-type';
config.label = 'List Style Type';
config.ariaLabel = 'List Style Type';

config.options= [
    {label: '', value: 'none', type: 'none'},

    {optgroup: true, label: 'Symbol', value: 'ul', type: 'ul'},
    {label: 'disc', value: 'disc', type: 'ul'},
    {label: 'circle', value: 'circle', type: 'ul'},
    {label: 'square', value: 'square', type: 'ul'},
    {label: 'custom', value: 'custom', type: 'ul'},

    {optgroup: true, label: 'Ordered', value: 'ol', type: 'ol'},
    {label: 'decimal', value: 'decimal', type: 'ol'},
    {label: 'decimal-leading-zero', value: 'decimal-leading-zero', type: 'ol'},
    {label: 'lower-roman', value: 'lower-roman', type: 'ol'},
    {label: 'upper-roman', value: 'upper-roman', type: 'ol'},
    {label: 'lower-alpha', value: 'lower-alpha', type: 'ol'},
    {label: 'upper-alpha', value: 'upper-alpha', type: 'ol'},

    {optgroup: true, label: 'Ordered - Other', value: 'ol-oth', type: 'ol_oth'},
    {label: 'lower-greek', value: 'lower-greek', type: 'ol_oth'},
    {label: 'lower-latin', value: 'lower-latin', type: 'ol_oth'},
    {label: 'upper-latin', value: 'upper-latin', type: 'ol_oth'},
    {label: 'armenian', value: 'armenian', type: 'ol_oth'},
    {label: 'georgian', value: 'georgian', type: 'ol_oth'},


];

// config.defaultKey =
config.defaultOptionIndex = 0;
config.defaultValue = 'none';

config.typeInclude = ['none', 'ul', 'ol', 'ol_oth'];
config.optgroupInclude = [];
config.optgroupExclude = [];
config.optgroupDisabled = [];

config.optionInclude = [];
config.optionExclude = [];
config.optionDisabled = [];

// config.help =
config.classField = 'wpez-blocks-select list-style-type';
config.resetActive = true;
config.resetButtonText = 'Reset';
config.resetTooltip = false;

export default config;