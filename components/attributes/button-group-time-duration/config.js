//import icons from '../../lib/icons/group/group-block-width';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-time-duration';
config.label =  'Animation Duration';
config.labelAria =  'Animation Duration';

config.tooltipPosition =  'top center';

config.buttons = [
    {
        buttonText: 'None',
        tooltipText: __('Duration None', 'wpez-blocks'),
        key: 'none',
        value: 'none',
        icon: 'dismiss',
        className: 'wpez-blocks-btn-none'
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        buttonText: 'Duration: Shortest',
        tooltipText: __('Duration: Shortest', 'wpez-blocks'),
        key: 'shortest',
        value: 'shortest',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-shortest'
    },
    {
        buttonText: 'Duration: Shorter',
        tooltipText: __('Duration: Shorter', 'wpez-blocks'),
        key: 'shorter',
        value: 'shorter',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-shorter'
    },
    {
        buttonText: 'Duration: Short',
        tooltipText: __('Duration: Short', 'wpez-blocks'),
        key: 'short',
        value: 'short',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-short'
    },
    {
        buttonText: 'Duration: Long',
        tooltipText: __('Duration: Long', 'wpez-blocks'),
        key: 'long',
        value: 'long',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-long'
    },
    {
        buttonText: 'Duration: Longer',
        tooltipText: __('Duration: Longer', 'wpez-blocks'),
        key: 'longer',
        value: 'longer',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-longer'
    },
    {
        buttonText: 'Duration: Longest',
        tooltipText: __('Duration: Longest', 'wpez-blocks'),
        key: 'longest',
        value: 'longest',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-longest'
    }
];

config.defaultKey = 'none';
config.resetTooltip = 'Reset to Duration: None';
config.exclude = ['longest'];
config.disabled = [];

  export default config;
