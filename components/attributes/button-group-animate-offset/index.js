/**
 * External dependencies
 */
import ButtonGroupIconButtons from '../base/base-button-group-icon-buttons';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import config from './config'

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const {Component, Fragment} = wp.element;

/**
 * ButtonGroupSizes Component
 */
class ButtonGroupAnimateOffset extends Component {

    constructor() {
        super(...arguments);
    }

    render() {

        return (
            <Fragment>
                <ButtonGroupIconButtons
                    config={ config }
                    {...this.props}
                />
            </Fragment>
        );
    }
}

export default ButtonGroupAnimateOffset;
