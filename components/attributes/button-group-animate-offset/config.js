//import icons from '../../lib/icons/group/group-block-width';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-animate-offset';
config.label =  'Animate Offset';
config.labelAria =  'Animate Offset';

config.exclude = [];
config.tooltipPosition =  'top center';

config.buttons = [

    {
        buttonText: __('Minus: Most', 'wpez-blocks'),
        tooltipText: __('Minus: Most', 'wpez-blocks'),
        key: 'minus_most',
        value: 'minus_most',
        icon: 'arrow-left',
        className: 'wpez-blocks-btn-minus-most'
    },
    {
        buttonText: __('Minus: More', 'wpez-blocks'),
        tooltipText: __('Minus: More', 'wpez-blocks'),
        key: 'minus_more',
        value: 'minus_more',
        icon: 'arrow-left',
        className: 'wpez-blocks-btn-minus-more'
    },
    {
        buttonText: __('Minus: Some', 'wpez-blocks'),
        tooltipText: __('Minus: Some', 'wpez-blocks'),
        key: 'minus_some',
        value: 'minus_some',
        icon: 'arrow-left',
        className: 'wpez-blocks-btn-minus-some'
    },
    {
        buttonText: __('Offset: None', 'wpez-blocks'),
        tooltipText: __('Offset: None', 'wpez-blocks'),
        key: 'none',
        value: 'none',
        icon: 'marker',
        className: 'wpez-blocks-btn-none'
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        buttonText: __('Plus: Some', 'wpez-blocks'),
        tooltipText: __('Plus: Some', 'wpez-blocks'),
        key: 'plus_some',
        value: 'plus_some',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-plus-some'
    },
    {
        buttonText: __('Plus: More', 'wpez-blocks'),
        tooltipText: __('Plus: More', 'wpez-blocks'),
        key: 'plus_more',
        value: 'plus_more',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-plus-more'
    },
    {
        buttonText: __('Plus: Most', 'wpez-blocks'),
        tooltipText: __('Plus: Most', 'wpez-blocks'),
        key: 'plus_most',
        value: 'plus_most',
        icon: 'arrow-right',
        className: 'wpez-blocks-btn-plus-most'
    }

];

config.defaultKey = 'none';
config.resetTooltip = 'Reset to Offset: None';
config.exclude = [];
config.disabled = [];


  export default config;
