const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-sizes';
config.label = 'Basic Buttons Sizes';
config.ariaLabel = 'Basic Buttons Sizes';


//config.buttons = [
    // buttonText: ,
    // ariaLabel  << if empty will default to tootipText
    // tooltipText: ,  << optional
    // tooltipPosition: , << optional
    // key: ,   // required
    // value: , // required
    // default: bool,  << only specify once
    // disabled: bool, << optional
    // className:
//];

config.buttons = [
    {
        tooltipText: __('XX-Small', 'wpez-blocks'),
        key: 'xxs',
        value: 'xxs',
        buttonText: __('XXS', 'wpez-blocks'),
    },
    {
        tooltipText: __('X-Small', 'wpez-blocks'),
        key: 'xs',
        value: 'xs',
        buttonText: __('XS', 'wpez-blocks'),
    },
    {
        tooltipText: __('Small', 'wpez-blocks'),
        key: 'sm',
        value: 'sm',
        buttonText:  __('S', 'wpez-blocks'),
    },
    {
        tooltipText: __('Medium', 'wpez-blocks'),
        key: 'md',
        value: 'md',
        buttonText: __('M', 'wpez-blocks'),
    },
    {
        tooltipText: __('Large', 'wpez-blocks'),
        key: 'lg',
        value: 'lg',
        buttonText: __('L', 'wpez-blocks'),
    },
    {
        tooltipText: __('X-Large', 'wpez-blocks'),
        key: 'xl',
        value: 'xl',
        buttonText: __('XL', 'wpez-blocks'),
    },
    {
        tooltipText: __('XX-Large', 'wpez-blocks'),
        key: 'xxl',
        value: 'xxl',
        buttonText: __('XXL', 'wpez-blocks'),
    }
];

config.defaultButtonIndex = 0;
config.defaultKey = 'md';
config.resetTooltip = 'Reset to M - Medium';

config.rowClassName = 'button-key-%s';
config.exclude = [];
config.disabled = [];
// config.help =

export default config;
