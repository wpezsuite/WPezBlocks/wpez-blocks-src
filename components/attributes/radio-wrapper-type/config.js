
// const { __ } = wp.i18n;

const config = {};

config.name = 'radio-wrapper-type';
config.label = 'Wrapper Type';
config.ariaLabel = 'Wrapper Type';

config.options= [
    {label: 'Basic', value: 'wt0', type: 'na'},
    {label: 'Plus', value: 'wt10', type: 'na'},
];

// config.defaultKey =
config.defaultOptionIndex = 0;
config.defaultValue = 'wt0';

config.typeInclude = ['na'];
config.optgroupInclude = [];
config.optgroupExclude = [];
config.optgroupDisabled = [];

config.optionInclude = [];
config.optionExclude = [];
config.optionDisabled = [];

// config.help =
config.classField = 'wpez-blocks-radio wrapper-type';
config.resetActive = true;
config.resetButtonText = 'Reset';
config.resetTooltip = false;

export default config;