/**
 * External dependencies
 */
import Radio from '../base/base-radio';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import config from './config'

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const {Component, Fragment} = wp.element;

/**
 * ButtonGroupSizes Component
 */
class RadioWrapperType extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        return (
            <Fragment>
                <Radio
                    config={ config }
                    {...this.props}
                />
            </Fragment>
        );
    }
}

export default RadioWrapperType;