
// const { __ } = wp.i18n;

const config = {};

config.name = 'select-aos-easing';
config.label = 'Animate Easing';
config.ariaLabel = 'Animate Easing';

config.options= [

    {label: 'ease', value: 'ease', type: 'general'},
    {label: 'linear', value: 'linear', type: 'general'},
    {label: 'ease-in', value: 'ease-in', type: 'general'},
    {label: 'ease-out', value: 'ease-out', type: 'general'},
    {label: 'ease-in-out', value: 'ease-in-out', type: 'general'},
 
    {label: 'ease-in-back', value: 'ease-in-back', type: 'back'},
    {label: 'ease-out-back', value: 'ease-out-back', type: 'back'},
    {label: 'ease-in-out-back', value: 'ease-in-out-back', type: 'back'},

    {label: 'ease-in-sine', value: 'ease-in-sine', type: 'sine'},
    {label: 'ease-out-sine', value: 'ease-out-sine', type: 'sine'},
    {label: 'ease-in-out-sine', value: 'ease-in-out-sine', type: 'sine'},

    {label: 'ease-in-quad', value: 'ease-in-quad', type: 'quad'},
    {label: 'ease-out-quad', value: 'ease-out-quad', type: 'quad'},
    {label: 'ease-in-out-quad', value: 'ease-in-out-quad', type: 'quad'},

    {label: 'ease-in-cubic', value: 'ease-in-cubic', type: 'cubic'},
    {label: 'ease-out-cubic', value: 'ease-out-cubic', type: 'cubic'},
    {label: 'ease-in-out-cubic', value: 'ease-in-out-cubic', type: 'cubic'},

    {label: 'ease-in-quart', value: 'ease-in-quart', type: 'quart'},
    {label: 'ease-out-quart', value: 'ease-out-quart', type: 'quart'},
    {label: 'ease-in-out-quart', value: 'ease-in-out-quart', type: 'quart'},

];

// config.defaultKey =
config.defaultOptionIndex = 0;
config.defaultValue = 'ease';

config.typeInclude = ['general', 'back', 'sine', 'quad', 'cubic', 'quart'];
config.optgroupInclude = [];
config.optgroupExclude = [];
config.optgroupDisabled = [];

config.optionInclude = [];
config.optionExclude = [];
config.optionDisabled = [];

// config.help =
config.classField = 'wpez-blocks-select aos-easing';
config.resetActive = true;
config.resetButtonText = 'Reset';
config.resetTooltip = false;

export default config;