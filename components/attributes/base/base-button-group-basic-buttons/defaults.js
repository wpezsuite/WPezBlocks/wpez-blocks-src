const { __ } = wp.i18n;

const defaults = {};

defaults.type = 'components';
defaults.name = 'base-button-group-basic-buttons';
defaults.label = 'Basic Buttons Base';
defaults.ariaLabel = 'Basic Buttons Base';


defaults.buttons = [
    // buttonText: ,
    // ariaLabel  << if empty will default to tootipText
    // tooltipText: ,  << optional
    // tooltipPosition: , << optional
    // key: ,   // required
    // value: , // required
    // default: bool,  << only specify once
    // disabled: bool, << optional
    // className:
];

// defaults.defaultKey =
defaults.rowClassName = 'button-key-%s';
defaults.exclude = [];
defaults.disabled = [];
// defaults.help =
defaults.classField = 'wpez-blocks-button-group basic-buttons';

// TODO - do this instead w/ CSS and a sibling selector
defaults.arrMarginRight = ['xs','sm', 'md', 'lg'];
defaults.marginRight = 'sm';
defaults.arrPadding = ['sm', 'md', 'lg'];
defaults.padding = 'md';

defaults.tooltipActive = true;
defaults.tooltipPosition = 'top center';
defaults.resetActive = true;
defaults.resetButtonText = 'Reset';
defaults.resetTooltip = false;
defaults.resetTooltipPosition = 'top left';

defaults.defaultButtonIndex = 0;

export default defaults;
