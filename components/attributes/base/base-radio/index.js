/**
 * External dependencies
 */
import wpezUtils from '../../../../lib/js/wpez-utils';
import map from 'lodash/map';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import globalDefaults from '../../../defaults';
import defaults from './defaults'

/**
 * WordPress dependencies
 */
const {__, sprintf} = wp.i18n;
const {Component, Fragment} = wp.element;
const {IconButton} = wp.components;
const {withInstanceId} = wp.compose;

/**
 * ButtonGroupSizes Component
 */
class Radio extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        const {
            config,

            instanceId, // NEW
            name,
            label,
            ariaLegend,
            options,
            defaultValue,

            typeInclude,
            optgroupInclude,
            optgroupExclude,
            optgroupDisabled,
            optionInclude,
            optionExclude,
            optionDisabled,

            help,
            classField = '',

            resetActive = true,
            resetButtonText = 'Reset',
            resetTooltip = 'Reset Tooltip',
            resetTooltipPosition,

            onChange,
            value,
        } = this.props;

        let thisOptions = (options) ? Array.isArray(options) ? options : (config.options || defaults.options) : (config.options || defaults.options);

        if (!thisOptions.length) {
            return null;
        }
        const id = `inspector-select-control-${instanceId}`;
        const thisName = name || config.name || defaults.name;
        const thisLabel = label || config.label || defaults.label;
        const thisAriaLegend = ariaLegend || config.ariaLegend || defaults.ariaLegend;
        const thisDefaultValue = defaultValue || config.defaultValue || defaults.defaultValue;
        const thisHelp = help || config.help || defaults.help;

        //
        const thisTypeInclude = (typeInclude) ? Array.isArray(typeInclude) ? typeInclude : (config.typeInclude || defaults.typeInclude) : (config.typeInclude || defaults.typeInclude);
        const thisOptgroupInclude = (optgroupInclude) ? Array.isArray(optgroupInclude) ? optgroupInclude : (config.optgroupInclude || defaults.optgroupInclude) : (config.optgroupInclude || defaults.optgroupInclude);
        const thisOptgroupExclude = (optgroupExclude) ? Array.isArray(optgroupExclude) ? optgroupExclude : (config.optgroupExclude || defaults.optgroupExclude) : (config.optgroupExclude || defaults.optgroupExclude);
        let arrOptgroupInEx = [];
        let flagOptgroupInEx = false;
        if (Array.isArray(thisOptgroupInclude) && thisOptgroupInclude.length) {
            arrOptgroupInEx = thisOptgroupInclude;
            flagOptgroupInEx = true;
        }
        if (!arrOptgroupInEx.length && Array.isArray(thisOptgroupExclude) && thisOptgroupExclude.length) {
            arrOptgroupInEx = thisOptgroupExclude;
            flagOptgroupInEx = false;
        }
        const thisOptgroupDisabled = (optgroupDisabled) ? Array.isArray(optgroupDisabled) ? optgroupDisabled : (config.optgroupDisabled || defaults.optgroupDisabled) : (config.optgroupDisabled || defaults.optgroupDisabled);
        //
        const thisOptionInclude = (optionInclude) ? Array.isArray(optionInclude) ? optionInclude : (config.optionInclude || defaults.optionInclude) : (config.optionInclude || defaults.optionInclude);
        const thisOptionExclude = (optionExclude) ? Array.isArray(optionExclude) ? optionExclude : (config.optionExclude || defaults.optionExclude) : (config.optionExclude || defaults.optionExclude);
        let arrOptionInEx = [];
        let flagOptionInEx = false;
        if (Array.isArray(thisOptionInclude) && thisOptionInclude.length) {
            arrOptionInEx = thisOptionInclude;
            flagOptionInEx = true;
        }
        if (!arrOptionInEx.length && Array.isArray(thisOptionExclude) && thisOptionExclude.length) {
            arrOptionInEx = thisOptionExclude;
            flagOptionInEx = false;
        }
        const thisOptionDisabled = (optionDisabled) ? Array.isArray(optionDisabled) ? optionDisabled : (config.optionDisabled || defaults.optionDisabled) : (config.optionDisabled || defaults.optionDisabled);

        const thisResetActive = resetActive || config.resetActive || defaults.resetActive;
        const thisResetButtonText = resetButtonText || config.resetButtonText || defaults.resetButtonText;
        const thisResetTooltip = resetTooltip || config.resetTooltip || defaults.resetTooltip;
        const thisResetTooltipPosition = resetTooltipPosition || config.resetTooltipPosition || defaults.resetTooltipPosition;


        // button w/ default: true. (else) button w/ key = defaultValue (else) button[defaultButtonIndex]
        const defaultOption = (thisOptions.find((option) => option.default === true)) ? thisOptions.find((option) => option.default === true) : (thisDefaultValue) ? thisOptions.find((option) => option.value === thisDefaultValue) : thisOptions[(config.defaultOptionIndex || defaults.defaultOptionIndex)];
        const currentOption = (value) ? thisOptions.find((option) => option.value === value.toString()) : defaultOption;
        const defaultOptionValue = defaultOption.value;
        // default to the default value
        (value) ? '' : onChange(defaultOptionValue);
        // const currentButtonName = (currentButton && currentButton.name) || config.labelCustom;

        // -------------------------
        const classesField = wpezUtils.classenames('field', defaults.type, thisName, [classField, config.classField, defaults.classField]);
        const classesLabel = wpezUtils.classenames('label', defaults.type, thisName);
        const classesHelp = wpezUtils.classenames('help', defaults.type, thisName);
        // const classesItemWrapper = wpezUtils.classenames('item_wrapper', defaults.type, thisName );

        const onChangeValue = (event) => {
            onChange(event.target.value);
        };

        if (thisTypeInclude.length > 0) {

            thisOptions = thisOptions.filter(function (opts) {
                if (thisTypeInclude.includes(opts.type)) {
                    return true;
                }
            })
        }

        return (

            <Fragment>
                <div className={classesField}>
                    <fieldset>
                        {thisAriaLegend &&
                        <legend className="sr-only">{thisAriaLegend}</legend>
                        }
                        {(thisLabel || thisResetActive) &&
                        <div className={globalDefaults.labelResetWrapperClassName}>
                            {thisLabel &&
                            <span className={classesLabel}>{thisLabel}</span>
                            }

                            {(thisResetActive && value !== thisDefaultValue) &&
                            <IconButton
                                tooltip={(thisResetTooltip)}
                                labelPosition={thisResetTooltipPosition}
                                aria-label={(thisResetTooltip) ? thisResetTooltip : thisResetButtonText}
                                className={globalDefaults.resetClassName}
                                type="button"
                                disabled={value === undefined}
                                aria-disabled={value === undefined}
                                onClick={() => onChange(defaultValue)}
                                isSmall
                                isDefault
                            >
                                {thisResetButtonText}
                            </IconButton>
                            }
                        </div>
                        }

                        <ul className={"wrapper-display-block"}>

                        {map(thisOptions, (row, i) => {
                            const isRendered = arrOptionInEx.includes(row.value) === flagOptionInEx;
                            const isDisabled = thisOptionDisabled.includes(row.value);

                            if (isRendered) {

                                return (
                                    <Fragment>
                                        <li>
                                        <input

                                            id={ `${ id }-${ i }` }
                                            className="components-radio-control__input"
                                            type="radio"
                                            name={ id }
                                            onChange={ onChangeValue }
                                            aria-describedby={ !! help ? `${ id }__help` : undefined }

                                            checked={value === row.value}
                                            disabled={isDisabled}
                                            aria-disabled={isDisabled}
                                            key={`${row.label}-${row.value}-${i}`}
                                            value={row.value}
                                        />
                                        <label htmlFor={ `${ id }-${ i }` }>
                                            {row.label}
                                        </label>
                                        </li>
                                    </Fragment>
                                )
                            }

                        })}
                        </ul>

                        {thisHelp && <p id={id + '__help'} className={classesHelp}>{thisHelp}</p>}
                    </fieldset>
                </div>
            </Fragment>
        )
    }
}

export default withInstanceId(Radio);
