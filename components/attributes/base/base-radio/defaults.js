
// const { __ } = wp.i18n;

const defaults = {};

defaults.type = 'components';
defaults.name = 'base-radio';
defaults.label = 'Radio Base';
defaults.ariaLegend = 'Radio Base';

defaults.options= [];
// defaults.defaultValue =
defaults.defaultOptionIndex = 0;

defaults.typeInclude = [];
defaults.optgroupInclude = [];
defaults.optgroupExclude = [];
defaults.optgroupDisabled = [];

defaults.optionInclude = [];
defaults.optionExclude = [];
defaults.optionDisabled = [];

defaults.disabled = [];
defaults.help = false;
defaults.classField = 'wpez-blocks-radio';

defaults.withReset = true;
defaults.withResetButtonText = 'Reset';
defaults.withResetTooltip = false;



export default defaults;
