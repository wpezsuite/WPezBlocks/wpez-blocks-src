// const { __ } = wp.i18n;

const defaults = {};

defaults.type = 'components';
defaults.name = 'base-range-picker';
defaults.classField= 'wpez-blocks-range-picker';
defaults.label = 'RangePicker';
defaults.ariaLabel = 'RangePicker - Customize';


defaults.buttons = [

    // buttonText: ,
    // buttonTextActive: bool
    // ariaLabel  << if empty will default to tootipText
    // tooltipText: ,  << optional
    // tooltipPosition: , << optional
    // key: ,
    // value: , << integer - will be displayed in the number  input
    // icon: ,  << optional
    // default: bool,  << only once
    // disabled: bool, << optional
    // className:
    // numberDisabled:  bool <<
];
defaults.exclude = [];  // array of keys to be excluded
defaults.disabled = [];  // array of keys that will disabled=true
defaults.rowClassName = 'button-key-%s';
defaults.tooltipActive = true;
defaults.tooltipPosition = 'top center';
defaults.numberActive = true;
defaults.numbersDisabled = [];  // array of keys that will disabled number input when key is selected.
defaults.resetActive = true;
defaults.resetButtonText = 'Reset';
defaults.resetTooltip = false;
defaults.sliderActive = false;

defaults.buttonTextActive = true;
defaults.min = 0;
defaults.max = 0;
defaults.defaultButtonIndex = 0;
defaults.labelCustom = 'Custom';
defaults.iconBefore = '';
defaults.iconAfter = '';
defaults.iconSaved = 'saved';

export default defaults;
