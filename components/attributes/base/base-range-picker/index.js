/**
 * External dependencies
 */
import wpezUtils from '../../../../lib/js/wpez-utils';
import {map} from 'lodash';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import globalDefaults from '../../../defaults';
import defaults from './defaults';

/**
 * WP dependencies
 */
const {__, sprintf} = wp.i18n;
const {Component, Fragment} = wp.element;
const {RangeControl, NavigableMenu, Button, IconButton, Dropdown, Dashicon} = wp.components;

/**
 * RangePicker Component
 */
class RangePicker extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        const {
            config,

            name,
            label,
            ariaLabel,
            buttons,
            defaultKey,
            rowClassName,
            exclude,
            disabled,
            help,
            classField = '',

            tooltipActive,
            tooltipPosition,
            resetActive,
            resetButtonText,
            resetTooltip,
            resetTooltipPosition,
            savedIcon,
            //
            buttonTextActive,
            numberActive,
            numbersDisabled,
            sliderActive,
            min,
            max,
            beforeIcon,
            afterIcon,

            onChange,
            value,
        } = this.props;

        const thisButtons = (buttons) ? Array.isArray(buttons) ? buttons : (config.buttons || defaults.buttons) : (config.buttons || defaults.buttons);

        if (!thisButtons.length) {
            return null;
        }

        // ----------------

        const thisName = name || config.name || defaults.name;
        const thisLabel = label || config.label || defaults.label;
        const thisAriaLabel = ariaLabel || config.ariaLabel || defaults.ariaLabel;
        //  buttons,
        const thisDefaultKey = defaultKey || config.defaultKey || defaults.defaultKey;
        const thisRowClassName = rowClassName || config.rowClassName || defaults.rowClassName;
        // exclude,
        const thisExclude = (exclude) ? Array.isArray(exclude) ? exclude : (config.exclude || defaults.exclude) : (config.exclude || defaults.exclude);
        //  disabled,
        const thisDisabled = (disabled) ? Array.isArray(disabled) ? disabled : (config.disabled || defaults.disabled) : (config.disabled || defaults.disabled);
        const thisHelp = help || config.help || defaults.help;
        // classField
        const thisTooltipActive = tooltipActive || config.tooltipActive || defaults.tooltipActive;
        const thisTooltipPosition = tooltipPosition || config.tooltipPosition || defaults.tooltipPosition;
        const thisResetActive = resetActive || config.resetActive || defaults.resetActive;
        const thisResetButtonText = resetButtonText || config.resetButtonText || defaults.resetButtonText;
        const thisResetTooltip = resetTooltip || config.resetTooltip || defaults.resetTooltip;
        const thisResetTooltipPosition = resetTooltipPosition || config.resetTooltipPosition || defaults.resetTooltipPosition;

        const thisSavedIcon = savedIcon || config.savedIcon || defaults.savedIcon;
        //
        const thisButtonTextActive = buttonTextActive || config.buttonTextActive || defaults.buttonTextActive;
        const thisNumberActive = numberActive || config.numberActive || defaults.numberActive;

        const thisNumbersDisabled = numbersDisabled || config.numbersDisabled || defaults.numbersDisabled;

        const thisSliderActive = sliderActive || config.sliderActive || defaults.sliderActive;
        const thisMin = min || config.min || defaults.min;
        const thisMax = max || config.max || defaults.max;
        const thisBeforeIcon = beforeIcon || config.beforeIcon || defaults.beforeIcon;
        const thisAfterIcon = afterIcon || config.afterIcon || defaults.afterIcon;

        // button w/ default: true. (else) button w/ key = thisDefaultKey (else) button[defaultButtonIndex]
        const defaultButton = (thisButtons.find((button) => button.default === true)) ? thisButtons.find((button) => button.default === true) : (thisDefaultKey) ? thisButtons.find((button) => button.key === thisDefaultKey) : thisButtons[(config.defaultButtonIndex || defaults.defaultButtonIndex)];
        const currentButton = (value) ? thisButtons.find((button) => button.key === value.toString()) : defaultButton;
        const defaultValue = defaultButton.value;
        // default to the default value
        (value) ? '' : onChange(defaultValue);
        const currentButtonName = (currentButton && currentButton.buttonText) || config.labelCustom || defaults.labelCustom;

        // check withNumbersDisabled and button.withNumbersDisabled
        const numDisActive = ((value) ? thisNumbersDisabled.includes(value) : false) || ((currentButton && currentButton.hasOwnProperty('numberDisabled')) ? currentButton.numberDisabled : false);

        const classesField = wpezUtils.classenames('field', defaults.type, thisName, [classField, config.classField, defaults.classField]);
        const classesLabel = wpezUtils.classenames('label', defaults.type, thisName);
        const classesHelp = wpezUtils.classenames('help', defaults.type, thisName);

        //  const classesItemWrapper = wpezUtils.classenames('item_wrapper', config.type, thisName);

        const onChangeValue = (event) => {
            const newValue = event.target.value;
            if (newValue === '') {
                onChange(undefined);
                return;
            }

            if (thisMin <= Number(newValue) && Number(newValue) <= thisMax) {
                let getButton = thisButtons.find((button) => button.value == newValue);
                onChange(getButton.key);
            }
        };

        return (
            <Fragment>
                <div className={classesField}>
                    { (thisLabel || thisResetActive ) &&
                    <div className={globalDefaults.labelResetWrapperClassName}>
                        {thisLabel &&
                        <span className={classesLabel}>{thisLabel}</span>
                        }

                        {(thisResetActive && value !== defaultValue) &&
                        <IconButton
                            tooltip = { (thisResetTooltip) }
                            labelPosition={thisResetTooltipPosition}
                            aria-label = { (thisResetTooltip) ? thisResetTooltip : thisResetButtonText}
                            className={globalDefaults.resetClassName}
                            type="button"
                            disabled={value === undefined}
                            aria-disabled={value === undefined}
                            onClick={() => onChange(defaultValue)}
                            isSmall
                            isDefault
                        >
                            {thisResetButtonText}
                        </IconButton>
                        }
                    </div>
                    }

                    <div className={thisName + "__buttons components-range-picker__buttons"}>
                        {(thisButtons.length > 0) &&
                        <Dropdown
                            className={thisName + "__dropdown components-range-picker__dropdown"}
                            contentClassName={thisName + "__dropdown-content components-range-picker__dropdown-content"}
                            position="bottom"
                            renderToggle={({isOpen, onToggle}) => (
                                <Button
                                    className="components-range-picker__selector"
                                    isLarge
                                    onClick={onToggle}
                                    aria-expanded={isOpen}
                                    aria-label={thisAriaLabel + ' ' + currentButtonName}
                                >
                                    {currentButtonName}
                                </Button>
                            )}
                            renderContent={() => (
                                <NavigableMenu>
                                    {map(thisButtons, (row, i) => {
                                        if (row.key && !thisExclude.includes(row.key)) {
                                            const isSelected = (value) ? row.key === value.toString() : row.value === defaultValue;
                                            const labelPosition = (row.tooltipPosition) ? row.tooltipPosition : thisTooltipPosition;
                                            const classNameRow = (row.className) ? row.className : sprintf(thisRowClassName, row.key);
                                            let tooltipText = '';
                                            if (thisTooltipActive === true) {
                                                tooltipText = (row.tooltipText) ? row.tooltipText : '';
                                            }
                                            let buttonText = (row.buttonText) ? row.buttonText : '';
                                            if (thisButtonTextActive === false || row.buttonTextActive === false) {
                                                buttonText = '';
                                            }
                                            return (

                                                <IconButton
                                                    icon={(row.icon) ? row.icon : ''}
                                                    tooltip={tooltipText}
                                                    labelPosition={labelPosition}
                                                    key={row.key}
                                                    onClick={() => onChange(row.key)}
                                                    className={classNameRow}
                                                    role="menuitemradio"
                                                    aria-checked={isSelected}
                                                    disabled={(row.disabled) ? row.disabled : thisDisabled.includes(row.key)}
                                                    aria-disabled={(row.disabled) ? row.disabled : thisDisabled.includes(row.key)}
                                                    isPrimary={isSelected}
                                                >
                                                    {(isSelected && thisSavedIcon) &&
                                                    <Dashicon icon={thisSavedIcon}/>
                                                    }
                                                    <span className="components-button-text__wrapper"
                                                        //    style={{width: row.width}}
                                                    >
                                                                 {buttonText}
                                                        </span>
                                                </IconButton>

                                            );

                                        }
                                    })}
                                </NavigableMenu>
                            )}
                        />
                        }
                        {(!thisSliderActive && thisNumberActive) &&
                        <input
                            className="components-range-control__number"
                            type="number"
                            onChange={onChangeValue}
                            disabled={numDisActive || value === undefined}
                            aria-label={__(thisAriaLabel)}
                            value={(value) ? (value.toString() === (parseInt(value, 10)).toString()) ? value : '' : defaultValue}
                        />
                        }

                        {(false && thisResetActive) &&
                        <Button
                            className="components-color-palette__clear"
                            type="button"
                            disabled={value === undefined}
                            onClick={() => onChange(defaultValue)}
                            isSmall
                            isDefault
                        >
                            {thisResetButtonText}
                        </Button>
                        }

                    </div>
                    {(thisSliderActive) &&
                    <RangeControl
                        className="components-range-picker__custom-input"
                        //    label={__('Custom Size')}
                        disabled={numDisActive || value === undefined}
                        value={(value) ? (value.toString() === (parseInt(value, 10)).toString()) ? value : '' : defaultValue}
                        initialPosition={(value) ? (value.toString() === (parseInt(value, 10)).toString()) ? value : '' : defaultValue}
                        onChange={onChange}
                        min={thisMin}
                        max={thisMax}
                        beforeIcon={thisBeforeIcon}
                        afterIcon={thisAfterIcon}
                    />
                    }
                    {thisHelp && <p className={classesHelp}>{thisHelp}</p>}
                </div>
            </Fragment>
        );
    }

}

export default RangePicker;