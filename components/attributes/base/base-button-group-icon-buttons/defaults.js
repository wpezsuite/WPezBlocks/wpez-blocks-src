// const { __ } = wp.i18n;

const defaults = {};

defaults.type = 'components';
defaults.name = 'base-button-group-icons-buttons';
defaults.label = 'Icon Buttons Base';
defaults.ariaLabel = 'Icon Buttons Base';

defaults.buttons = [
    // buttonText: ,
    // buttonTextActive: bool
    // ariaLabel  << if empty will default to tootipText
    // tooltipText: ,  << optional
    // tooltipPosition: , << optional
    // key: ,   // required
    // value: , // required
    // icon: ,
    // default: bool,  << only specify once for all buttons
    // disabled: bool, << optional
    // className:
    // dataSubscript:  << e.g. used for headings (H icon + subscript)
];

// defaults.defaultKey =
defaults.rowClassName = 'button-key-%s';
defaults.exclude = [];
defaults.disabled = [];
// defaults.help =
defaults.classField = 'wpez-blocks-button-group icon-buttons';
defaults.buttonTextActive = false;

defaults.tooltipActive = true;
defaults.tooltipPosition = 'top center';
defaults.resetActive = true;
defaults.resetButtonText = 'Reset';
defaults.resetTooltip = false;
defaults.resetTooltipPosition  = 'top left';

defaults.defaultButtonIndex = 0;
defaults.dataSubscript = false;
defaults.savedIcon = false;


export default defaults;
