/**
 * External dependencies
 */
import wpezUtils from '../../../../lib/js/wpez-utils';
import map from 'lodash/map';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import globalDefaults from '../../../defaults';
import defaults from './defaults'

/**
 * WordPress dependencies
 */
const {__, sprintf} = wp.i18n;
const {Component, Fragment} = wp.element;
const {ButtonGroup, IconButton} = wp.components;

/**
 * ButtonGroupSizes Component
 */
class ButtonGroupIconButtons extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        const {
            config,

            name,
            label,
            ariaLabel,
            buttons,
            defaultKey,
            rowClassName,
            exclude,
            disabled,
            help,
            classField = '',
            buttonTextActive,
            tooltipActive,
            tooltipPosition,
            resetActive,
            resetButtonText,
            resetTooltip,
            resetTooltipPosition,
            savedIcon,

            onChange,
            value,
            ...additionalProps
        } = this.props;

        const thisButtons = (buttons) ? Array.isArray(buttons) ? buttons : (config.buttons || defaults.buttons) : (config.buttons || defaults.buttons);

        if (!thisButtons.length) {
            return null;
        }

        const thisName = name || config.name || defaults.name;
        const thisLabel = label || config.label || defaults.label;
        const thisAriaLabel = ariaLabel || config.ariaLabel || defaults.ariaLabel;
        //  buttons,
        const thisDefaultKey = defaultKey || config.defaultKey || defaults.defaultKey;
        const thisRowClassName = rowClassName || config.rowClassName || defaults.rowClassName;
        // exclude,
        const thisExclude = (exclude) ? Array.isArray(exclude) ? exclude : (config.exclude || defaults.exclude) : (config.exclude || defaults.exclude);
        //  disabled,
        const thisDisabled = (disabled) ? Array.isArray(disabled) ? disabled : (config.disabled || defaults.disabled) : (config.disabled || defaults.disabled);
        const thisHelp = help || config.help || defaults.help;
        // classField
        const thisButtonTextActive = buttonTextActive || config.buttonTextActive || defaults.buttonTextActive;
        const thisTooltipActive = tooltipActive || config.tooltipActive || defaults.tooltipActive;
        const thisTooltipPosition = tooltipPosition || config.toolTipPosition || defaults.tooltipPosition;
        const thisResetActive = resetActive || config.resetActive || defaults.resetActive;
        const thisResetButtonText = resetButtonText || config.resetButtonText || defaults.resetButtonText;
        const thisResetTooltip = resetTooltip || config.resetTooltip || defaults.resetTooltip;
        const thisResetTooltipPosition = resetTooltipPosition || config.resetTooltipPosition || defaults.resetTooltipPosition;

        const thisSavedIcon = savedIcon || config.savedIcon || defaults.savedIcon;

        // button w/ default: true. (else) button w/ key = defaultKey (else) button[defaultButtonIndex]
        const defaultButton = (thisButtons.find((button) => button.default === true)) ? thisButtons.find((button) => button.default === true) : (thisDefaultKey) ? thisButtons.find((button) => button.key === thisDefaultKey) : thisButtons[(config.defaultButtonIndex || defaults.defaultButtonIndex)];
        const currentButton = (value) ? thisButtons.find((button) => button.key === value.toString()) : defaultButton;
        const defaultValue = defaultButton.value;
        // default to the default value
        (value) ? '' : onChange(defaultValue);
        // const currentButtonName = (currentButton && currentButton.name) || config.labelCustom;

        const classesField = wpezUtils.classenames('field', defaults.type, thisName, [classField, config.classField, defaults.classField]);
        const classesLabel = wpezUtils.classenames('label', defaults.type, thisName);
        const classesHelp = wpezUtils.classenames('help', defaults.type, thisName);
        const classesItemWrapper = wpezUtils.classenames('item_wrapper', defaults.type, thisName);

        return (
            <Fragment>
                <div className={classesField}>
                    {(thisLabel || thisResetActive) &&
                    <div className={globalDefaults.labelResetWrapperClassName}>
                        {thisLabel &&
                        <span className={classesLabel}>{thisLabel}</span>
                        }

                        {(thisResetActive && value !== defaultValue ) &&
                        <IconButton
                            tooltip={(thisResetTooltip)}
                            labelPosition={thisResetTooltipPosition}
                            aria-label={(thisResetTooltip) ? thisResetTooltip : thisResetButtonText}
                            className={globalDefaults.resetClassName}
                            type="button"
                            disabled={value === undefined}
                            aria-disabled={value === undefined}
                            onClick={() => onChange(defaultValue)}
                            isSmall
                            isDefault
                        >
                            {thisResetButtonText}
                        </IconButton>
                        }
                    </div>
                    }

                    <ButtonGroup aria-label={__(thisAriaLabel)}>
                        {map(thisButtons, (row, i) => {

                            if (row.key && row.value) {
                                if (!thisExclude.includes(row.key)) {

                                    const rowAriaLabel = (row.ariaLabel) ? row.ariaLabel : (row.tooltipText) ? row.tooltipText : '';
                                    const rowTooltipPosition = (row.tooltipPosition) ? row.tooltipPosition : thisTooltipPosition;
                                    const classNameRow = (row.className) ? row.className : sprintf(thisRowClassName, row.key);
                                    const dataSubscript = (row.dataSubscript) ? row.dataSubscript : (config.dataSubscript || defaults.dataSubscript);

                                    const isSelected = (value) ? row.key === value.toString() : row.value === defaultValue;
                                    const rowDisabled = (row.disabled) ? row.disabled : thisDisabled.includes(row.key);

                                    let tooltipText = '';
                                    if (thisTooltipActive === true) {
                                        tooltipText = (row.tooltipText) ? row.tooltipText : '';
                                    }
                                    let buttonText = (row.buttonText) ? row.buttonText : '';
                                    if (thisButtonTextActive === false || row.buttonTextActive === false) {
                                        buttonText = false;
                                    }

                                    // if we don't do this we get a CSS class of 'has-text' added even if {buttonText} is empty / false / etc.
                                    if (buttonText === false) {

                                        return (
                                            <span className={classesItemWrapper}>
                                            <IconButton
                                                icon={row.icon}
                                                tooltip={tooltipText}
                                                label={rowAriaLabel}
                                                labelPosition={rowTooltipPosition}
                                                key={row.key}
                                                className={classNameRow}
                                                disabled={rowDisabled}
                                                aria-disabled={rowDisabled}
                                                isPrimary={isSelected}
                                                aria-checked={isSelected}
                                                data-subscript={dataSubscript}
                                                onClick={() => onChange(row.key)}
                                            >
                                    {(isSelected && thisSavedIcon) &&
                                    <Dashicon icon={thisSavedIcon}/>
                                    }
                                </IconButton>
						</span>
                                        )
                                    } else {
                                        return (
                                            <span className={classesItemWrapper}>
                                            <IconButton
                                                icon={row.icon}
                                                tooltip={tooltipText}
                                                label={rowAriaLabel}
                                                labelPosition={rowTooltipPosition}
                                                key={row.key}
                                                className={classNameRow}
                                                disabled={rowDisabled}
                                                aria-disabled={rowDisabled}
                                                isPrimary={isSelected}
                                                aria-checked={isSelected}
                                                data-subscript={dataSubscript}
                                                onClick={() => onChange(row.key)}
                                            >
                                    {(isSelected && thisSavedIcon) &&
                                    <Dashicon icon={thisSavedIcon}/>
                                    }
                                                {buttonText &&
                                                <span className="components-button-text__wrapper"
                                                    //    style={{width: row.width}}
                                                >{buttonText}</span>
                                                }

                                </IconButton>
						</span>
                                        )

                                    }
                                }
                            }
                        })}
                    </ButtonGroup>
                    {thisHelp && <p className={classesHelp}>{thisHelp}</p>}
                </div>
            </Fragment>
        );
    }
}

export default ButtonGroupIconButtons;
