import icons from '../../../lib/icons/group/group-element-height';

const { __ } = wp.i18n;

const config = {};

config.name = 'button-group-element-height';
config.label =  'Height';
config.ariaLabel =  'Height';

config.buttons = [

    {
      //  buttonText:  __('Left-Aligned', 'wpez-blocks'),
        buttonTextActive: false,
        tooltipText: __('Height: Auto', 'wpez-blocks'),
        key: 'none',
        value: 'none',
        icon: icons.dismiss
        // ariaLabel: 'Optional, else the component uses the tooltipText',
        // tooltipPosition: 'top left',
    },
    {
        buttonText:  __('XXS', 'wpez-blocks'),
        tooltipText: __('Height: XX-Small', 'wpez-blocks'),
        key: 'xxs',
        value: 'xxs',
        icon: false
    },
    {
        buttonText:  __('XS', 'wpez-blocks'),
        tooltipText: __('Height: X-Small', 'wpez-blocks'),
        key: 'xs',
        value: 'xs',
        icon: false
    },
    {
        buttonText: __('S', 'wpez-blocks'),
        tooltipText: __('Height: Small', 'wpez-blocks'),
        key: 'sm',
        value: 'sm',
        icon: false
    },
    {
        buttonText:  __('M', 'wpez-blocks'),
        tooltipText: __('Height: Medium', 'wpez-blocks'),
        key: 'md',
        value: 'md',
        icon: false

    },
    {
        buttonText:  __('L', 'wpez-blocks'),
        tooltipText: __('Height: Large', 'wpez-blocks'),
        key: 'lg',
        value: 'lg',
        icon: false
    },
    {
        buttonText: __('XL', 'wpez-blocks'),
        tooltipText: __('Height: X-Large', 'wpez-blocks'),
        key: 'xl',
        value: 'xl',
        icon: false
    },
    {
        buttonText: __('XXL', 'wpez-blocks'),
        tooltipText: __('Height: XX-Large', 'wpez-blocks'),
        key: 'xxl',
        value: 'xxl',
        icon: false
    },

    {
        buttonText: __('Full', 'wpez-blocks'),
        tooltipText: __('Height: Full', 'wpez-blocks'),
        key: 'full',
        value: 'full',
        icon: false
    },

];

config.defaultKey = 'xs';
config.resetTooltip = 'Reset to X-Small';
config.buttonTextActive = true;
config.exclude = [];
config.disabled = [];

export default config;
