import icon from '../../../lib/icons/icon/heading';

const {__} = wp.i18n;

const config = {};

config.name = 'range-picker-width-columns';
config.label = 'Width: In Columns';
config.labelAria = 'Width: In Columns';

config.exclude = [];
config.disabled = [];
config.tooltipPosition = 'top center';
config.tooltipActive = true;
config.numberActive = true;
config.resetActive = true;
config.min = 1;
config.max = 12;

config.savedIcon = '';

config.buttons = [

    {
        buttonText: __('1', 'wpez-blocks'),
        tooltipText: __('8.33%', 'wpez-blocks'),
        key: '1',
        value: 1,
    },
    {
        buttonText: __('2', 'wpez-blocks'),
        tooltipText: __('16.67%', 'wpez-blocks'),
        key: '2',
        value: 2,
    },
    {
        buttonText: __('3', 'wpez-blocks'),
        tooltipText: __('25.00%', 'wpez-blocks'),
        key: '3',
        value: 3,
    },
    {
        buttonText: __('4', 'wpez-blocks'),
        tooltipText: __('33.33%', 'wpez-blocks'),
        key: '4',
        value: 4,
    },
    {
        buttonText: __('5', 'wpez-blocks'),
        tooltipText: __('41.67%', 'wpez-blocks'),
        key: '5',
        value: 5,
    },
    {
        buttonText: __('6', 'wpez-blocks'),
        tooltipText: __('50.00%', 'wpez-blocks'),
        key: '6',
        value: 6,
    },
    {
        buttonText: __('7', 'wpez-blocks'),
        tooltipText: __('58.33%', 'wpez-blocks'),
        key: '7',
        value: 7,
    },
    {
        buttonText: __('8', 'wpez-blocks'),
        tooltipText: __('66.67%', 'wpez-blocks'),
        key: '8',
        value: 8,
    },
    {
        buttonText: __('9', 'wpez-blocks'),
        tooltipText: __('75%', 'wpez-blocks'),
        key: '9',
        value: 9,
    },
    {
        buttonText: __('10', 'wpez-blocks'),
        tooltipText: __('83.33%', 'wpez-blocks'),
        key: '10',
        value: 10,
    },
    {
        buttonText: __('11', 'wpez-blocks'),
        tooltipText: __('91.67%', 'wpez-blocks'),
        key: '11',
        value: 11,
    },

    {
        buttonText: __('12', 'wpez-blocks'),
        tooltipText: __('100%', 'wpez-blocks'),
        key: '12',
        value: 12,
    },
];

config.defaultKey = '6';
config.resetTooltip = 'Reset to 6 columns';

export default config;
