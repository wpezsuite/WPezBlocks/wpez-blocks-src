/**
 * External dependencies
 */
import RangePicker from '../base/base-range-picker';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import config from './config'

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const {Component, Fragment} = wp.element;

/**
 * ButtonGroupSizes Component
 */
class RangePickerWidthColumns extends Component {

    constructor(props) {
        super(...arguments);
    }

    render() {

        return (
            <Fragment>
                <RangePicker
                    config={ config }
                    {...this.props}
                />
            </Fragment>
        );
    }
}

export default RangePickerWidthColumns;
