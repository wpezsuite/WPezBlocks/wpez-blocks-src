const animateRepeat = <svg viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
    <path d="m5 7v3l-2 1.5v-6.5h11v-2l4 3.01-4 2.99v-2h-9zm10 6v-3l2-1.5v6.5h-11v2l-4-3.01 4-2.99v2h9z"/>
</svg>

export default animateRepeat;