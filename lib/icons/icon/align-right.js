const alignRight = <svg viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
        <path d="m3 5h14v-2h-14v2zm0 4h3v-2h-3v2zm14 4v-6h-9v6h9zm-14 0h3v-2h-3v2zm0 4h14v-2h-14v2z"/>
    </svg>

    export default alignRight;
