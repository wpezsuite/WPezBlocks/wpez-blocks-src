const alignCenter = <svg viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
        <path d="m3 5h14v-2h-14v2zm12 8v-6h-10v6h10zm-12 4h14v-2h-14v2z"/>
    </svg>

export default alignCenter