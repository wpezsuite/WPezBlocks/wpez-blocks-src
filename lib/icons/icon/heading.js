const heading = <svg class="weab-svg" viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
    <polygon points="12.5 4 12.5 9.2 7.5 9.2 7.5 4 5 4 5 17 7.5 17 7.5 11.8 12.5 11.8 12.5 17 15 17 15 4"/>
</svg>

export default heading;