const alignBoxWidth = <svg viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
        <rect width="100%" height="100%" fill="none"/>
        <g className="currentLayer">
            <rect width="20" height="20" fill="none"/>
            <rect x="3.918" y="5.041" width="12.163" height="9.918"/>
            <rect x="-15" y="7" width="22" height="0"/>
        </g>
    </svg>

    export default alignBoxWidth;
