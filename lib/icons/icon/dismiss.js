const dismiss = <svg class="weab-svg" viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
    <path d="m10 2c4.42 0 8 3.58 8 8s-3.58 8-8 8-8-3.58-8-8 3.58-8 8-8zm5 11l-3-3 3-3-2-2-3 3-3-3-2 2 3 3-3 3 2 2 3-3 3 3z"/>
</svg>

export default dismiss;