const alignVertTop = <svg viewBox="0 0 20 20" width="20" height="20" xmlns="http://www.w3.org/2000/svg">
    <rect width="100%" height="100%" fill="none"/>
    <g className="currentLayer">
        <path d="m-36.51 0.26531h14v-2h-14v2zm12 8v-6h-10v6h10zm-12 4h14v-2h-14v2z" className="selected"/>
        <rect x="5" y="3" width="10" height="6"/>
        <rect x="8" y="9" width="4" height="0"/>
        <rect x="3.122" y="15.061" width="13.755" height="1.939"/>
        <rect x="3.122" y="11.224" width="13.755" height="1.939"/>
    </g>
</svg>;

export default alignVertTop;