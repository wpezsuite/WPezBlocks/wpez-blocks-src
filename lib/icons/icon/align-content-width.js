const alignContentWidth  = <svg viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" width='20' height='20'>
        <rect width="100%" height="100%" fill="none"/>
        <g className="currentLayer">
            <rect width="20" height="20" fill="none"/>
            <rect x="4.929" y="7.041" width="10.143" height="6"/>
            <rect x="10" y="5" width="0" height="2"/>
            <rect x="5" y="2.98" width="10" height="2"/>
            <rect x="5" y="15.061" width="10" height="2"/>
        </g>
    </svg>;

    export default alignContentWidth;
