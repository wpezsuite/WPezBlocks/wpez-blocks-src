import alignVertTop from '../icon/align-vert-top';
import alignVertCenter from '../icon/align-vert-center';
import alignVertBottom from '../icon/align-vert-bottom';

const icons = {};

icons.top = alignVertTop;
icons.center = alignVertCenter;
icons.bottom = alignVertBottom;

export default icons;