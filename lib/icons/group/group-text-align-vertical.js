import textTop from '../icon/text-align-vert-top';
import textCenter from '../icon/text-align-vert-center';
import textBottom from '../icon/text-align-vert-bottom';

const icons = {};

icons.top = textTop;
icons.center = textCenter;
icons.bottom = textBottom;

export default icons;