import alignBoxWidth from '../icon/align-box-width';
import alignContentWidth from '../icon/align-content-width';
import alignWide from '../icon/align-wide';
import alignFullWidth from '../icon/align-full-width';

const icons = {};

icons.box = alignBoxWidth;
icons.content = alignContentWidth;
icons.wide = alignWide;
icons.full = alignFullWidth;

export default icons;
