import dismiss from '../icon/dismiss';

const icons = {};

icons.dismiss = dismiss;

export default icons;