import textLeft from '../icon/text-align-left';
import textCenter from '../icon/text-align-center';
import textRight from '../icon/text-align-right';
import textJustify from '../icon/text-align-justify';

const icons = {};

icons.left = textLeft;
icons.center = textCenter;
icons.right = textRight;
icons.justify = textJustify;


export default icons;