import alignLeft from '../icon/align-left';
import alignCenter from '../icon/align-center-horizontal';
import alignRight from '../icon/align-right';

const icons = {};

icons.left = alignLeft;
icons.center = alignCenter;
icons.right = alignRight;


export default icons;