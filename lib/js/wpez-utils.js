import classnames from 'classnames';

const {getColorObjectByColorValue} = wp.editor;
const wpezUtils = {};


wpezUtils.bestBool = (bool1, key, arr1=[], arr2=[], fallback = true) => {

    if ( typeof bool1 !== 'undefined'){
        return bool1;
    }
    if ( typeof arr1[key] !== 'undefined'){
        return arr1[key];
    }
    if ( typeof arr2[key] !== 'undefined'){
        return arr2[key];
    }
    return fallback

};


wpezUtils.textClassnames = (blockName, className, attributes,  slug = 'wpez-blocks') => {

    const {

        featureSet,

        blockWidth,
        blockHeight,
        blockAnchor,

        boxWidth,
        boxHeight,
        boxAlign,
        boxAlignVert,

        blockAniName,
        blockAniDuration,
        boxAniName,
        boxAniDuration,
        contentAniName,
        contentAniDuration,

        textTagName,
        textListStyleType,
        textDropCap,
        textSize,
        textAlign,
        textAlignVert,

        colorComboBasic,
        colorCombo,

    } = attributes;

    let thisBlockAniName = (blockAniName)? blockAniName :  'none';
    let thisBlockAniDuration = (blockAniDuration)? blockAniDuration :  'none';
    let aniBlock = ( thisBlockAniName === 'none' || thisBlockAniDuration === 'none')? false : 'true';

    let thisBoxAniName = (boxAniName)? boxAniName :  'none';
    let thisBoxAniDuration = (boxAniDuration)? boxAniDuration :  'none';
    let aniBox = ( thisBoxAniName === 'none' || thisBoxAniDuration === 'none')? false : 'true';

    let thisContentAniName = (contentAniName)? contentAniName :  'none';
    let thisContentAniDuration = (contentAniDuration)? contentAniDuration :  'none';
    let aniContent = ( thisContentAniName === 'none' || thisContentAniDuration === 'none')? false : 'true';
    // TODO - add other ani*

    const aniClasses = (
        {
            [slug + `-aniBlock-${aniBlock}`]: aniBlock,
            [slug + `-aniBox-${aniBox}`]: aniBox,
            [slug + `-aniContent-${aniContent}`]: aniContent,

        });

   // const thisColorCombo = (featureSet < '25') ? colorComboBasic : colorCombo;

    return classnames(
        className, aniClasses,{

       //     [slug + '-block-wrapper' ]: true,
       //     [slug + '-rich-text']: true,
            [slug + '-isEditor-true']: true,
            [slug + `-blockName-${blockName}`]: blockName,
            [slug + `-featureSet-${featureSet}`]: featureSet,

            [slug + `-blockWidth-${blockWidth}`]: blockWidth,
            [slug + `-blockHeight-${blockHeight}`]: blockHeight,
            [slug + '-blockAnchor-true']: blockAnchor,
        //    [slug + '-blockAnchor-false']: !blockAnchor,

            [slug + `-boxWidth-${boxWidth}`]: boxWidth,
            [slug + `-boxHeight-${boxHeight}`]: boxHeight,
            [slug + `-boxAlign-${boxAlign}`]: boxAlign,
            [slug + `-boxAlignVert-${boxAlignVert}`]: boxAlignVert,

            [slug + `-textTagName-${textTagName}`]: textTagName,
            [slug + `-textListStyleType-${textListStyleType}`]: textListStyleType,
            [slug + `-textAlign-${textAlign}`]: textAlign,
            [slug + `-textAlignVert-${textAlignVert}`]: textAlignVert,
            [slug + `-textSize-${textSize}`]: textSize,
            [slug + `-textDropCap-${textDropCap}`]: textDropCap,

           [slug + `-colorComboBasic-${colorComboBasic}`]: colorComboBasic,
            [slug + `-colorCombo-${colorCombo}`]: colorCombo,

        //    [slug + `-colorCombo-${thisColorCombo}`]: thisColorCombo,

        });
};


wpezUtils.stringToBool = function stringToBool(val) {
    return (val === 'true') ? true : false
};

wpezUtils.isArray = function isArray(arr, fallback) {

    return (arr) ? Array.isArray(arr) ? arr : fallback : fallback;
};


wpezUtils.colorObject = function colorObject(colors, color, prop, undef = '') {

    var colorObj = getColorObjectByColorValue(colors, color);

    if (typeof colorObj === 'undefined') {
        // remove the # from the color so, worst case you'd still get a valid classname
        if (color) {
            return color.slice(1);
        }
        return undef;
    }

    switch (prop) {
        case 'color':
            if (colorObj.color) {
                return colorObj.color
            }
            return '';

        case 'name':
            if (colorObj.name) {
                return colorObj.name
            }
            return '';
        case 'slug':
            if (colorObj.slug) {
                return colorObj.slug
            }
            return ''

        default:
            return ''
    }
}

wpezUtils.classenames = function classnames(ele, type, name, arr) {

    const nspace = 'wpez-blocks'
    const slugBase = type + '-base-control'
    const slugClass = type + '-' + nspace

    var arr = (arr) ? Array.isArray(arr) ? arr : [] : [];

    switch (ele) {
        case 'field':
        case 'wrapper':
            arr.push(nspace);
            arr.push(nspace + '-' + type);
            break;

        case 'help':
            return 'wpez-blocks-help';

        case 'label':
            break;

        case 'item_wrapper':
            return name + '__' + ele + ' ' + ele;

        default:
            return ''
    }

    return classnames(
        slugBase + '__' + ele,
        slugClass + '-' + name + '__' + ele,
        arr, {}
    );
}


export default wpezUtils;
