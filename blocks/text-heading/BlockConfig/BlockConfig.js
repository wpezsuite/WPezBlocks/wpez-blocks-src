class BlockConfig{

    constructor( args = {} ){

        if ( typeof args === 'object'){
            this.args = args;

        } else {
            this.args = {};
        }

    }

    get(req = '') {

        if (typeof this.args[req] !== 'undefined' && typeof this.args[req] === 'object') {

            switch (req) {

                case 'inspector_PanelBodyCustomizeRichText':

                    if ( typeof this.args[req].attribute_textTagName === 'undefined' ) {
                        this.args[req].attribute_textTagName = {};
                        this.args[req].attribute_textTagName.active = true;
                    } else if ( typeof this.args[req].attribute_textTagName === 'object' ) {
                    this.args[req].attribute_textTagName.active = true;
                }

               //     console.log( '+++ ' + req);
                //    console.log( this.args[req]);


                    break;
                default:
              //      console.log( '----- ' + req);

            }

            return this.args[req];
        }
        // always return an object
        return {}
    }

}

export default BlockConfig