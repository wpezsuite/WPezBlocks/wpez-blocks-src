/**
 * External dependencies
 */

/**
 * Block dependencies
 */
import BlockConfig from './BlockConfig/BlockConfig';
import './styles/style.scss';
import blockBasics from "./block-basics";
import attributesInspector from "./attributes-inspector";
import attributesEdit from "./attributes-edit";
import TextHeading from './TextHeading';

/**
 * Internal block libraries
 */
//const {__} = wp.i18n;
const {registerBlockType, unregisterBlockType} = wp.blocks;
const {Fragment} = wp.element;

let extConfig = {};
if (typeof wpezBlocksTextHeading !== 'undefined' ) {
    extConfig = wpezBlocksTextHeading;
} else {
    console.log('No config: wpezBlocksTextHeading ');
}
const wpezBC = new BlockConfig(extConfig);
const thisBC = wpezBC.get('register__TextHeading');

const registerBlock =  thisBC.hasOwnProperty('active') ? thisBC.active :  true;

if ( registerBlock ) {

    /**
     * Register block
     */
    registerBlockType(
        blockBasics.name,
        {
            title: blockBasics.title,
            description: blockBasics.description,
            category: blockBasics.category,
            icon: blockBasics.icon,
            keywords: blockBasics.keywords,
            attributes: {
                ...attributesInspector,
                ...attributesEdit
            },

            edit: props => {

                return (

                    <Fragment>
                        <TextHeading
                            wpezBC={wpezBC}
                            {...props}
                        />
                    </Fragment>
                );
            },

            save: props => {
                return (null);
            }

        },
    );
}



