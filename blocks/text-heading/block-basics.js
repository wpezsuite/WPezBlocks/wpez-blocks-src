const {__} = wp.i18n;

import heading from '../../lib/icons/icon/heading'

const settingsBasics = {
    name: 'wpez-blocks/text-heading',
    title: __('Heading', 'wpez-blocks'),
    description: __('', 'wpez-blocks'),
    category: 'common',
    icon: {
        src: heading
    },
    keywords: [
        __('title', 'wpez-blocks'),
        __('subtitle', 'wpez-blocks'),
        __('headline', 'wpez-blocks'),
        __('wpez heading', 'wpez-blocks')
    ],
};

export default settingsBasics;