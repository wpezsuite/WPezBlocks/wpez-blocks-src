/**
 * External dependencies
 */
import map from "lodash/map";


function config( args ){

    if ( typeof args === 'undefined' || typeof args !== 'object'){
        args = {};

    }

    let obj = {};

    obj.blockName = 'text-heading';
    obj.rtsType = 'head';

    const initAttrs = [

        {attr: 'featureSet', val: '01'},
        {attr: 'blockWidth', val: 'content'},
        {attr: 'blockHeight', val: 'md'},
        {attr: 'blockAnchor', val: ''},
        {attr: 'boxWidth', val: '6'},
        {attr: 'boxHeight', val: 'md'},
        {attr: 'boxAlign', val: 'center'},
        {attr: 'boxAlignVert', val: 'center'},

        {attr: 'blockAniName', val: 'none'},
        {attr: 'blockAniDuration', val: 'none'},
        {attr: 'blockAniDelay', val: 'none'},
        {attr: 'blockAniEasing', val: 'ease'},
        {attr: 'blockAniEleAnchor', val: 'center'},
        {attr: 'blockAniWinAnchor', val: 'bottom'},
        {attr: 'blockAniOffset', val: 'none'},

        {attr: 'boxAniName', val: 'none'},
        {attr: 'boxAniDuration', val: 'none'},
        {attr: 'boxAniDelay', val: 'none'},
        {attr: 'boxAniEasing', val: 'ease'},
        {attr: 'boxAniEleAnchor', val: 'center'},
        {attr: 'boxAniWinAnchor', val: 'bottom'},
        {attr: 'boxAniOffset', val: 'none'},

        {attr: 'textTagName', val: 'h2'},
        {attr: 'textListStyleType', val: ''},
        {attr: 'textAlign', val: 'left'},
        {attr: 'textAlignVert', val: 'center'},
        {attr: 'textSize', val: 'md'},
        {attr: 'textDropCap', val: false},

        {attr: 'contentAniName', val: 'none'},
        {attr: 'contentDuration', val: 'none'},
        {attr: 'contentDelay', val: 'none'},
        {attr: 'contentAniEasing', val: 'ease'},
        {attr: 'contentAniEleAnchor', val: 'center'},
        {attr: 'contentAniWinAnchor', val: 'bottom'},
        {attr: 'contentAniOffset', val: 'none'},

        {attr: 'contentRichText', val: ''},

    ];

    let argsInitAttrs = {};
    if ( args.hasOwnProperty('initialize__attributes') && ( typeof args.initialize__attributes === 'object' ) ){
        argsInitAttrs =  args.initialize__attributes;
    }

    // for a given attribute (in initAttrs) is there a new value?
    map( initAttrs, (row, i) => {
         if (typeof  argsInitAttrs[row.attr] !== 'undefined') {
             row.val =  argsInitAttrs[row.attr];
         }
    });

    obj.initAttrs = initAttrs;

    return obj;
}

export default config;

