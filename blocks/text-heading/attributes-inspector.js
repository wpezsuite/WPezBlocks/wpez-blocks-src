import attributesWrapper from '../../components/inspector-controls/panel-body-wrapper/attributes';
import attributesCustomize from '../../components/inspector-controls/panel-body-rich-text-single-customize/attributes';
import attributesColor from '../../components/inspector-controls/panel-body-color-combos/attributes';

const attributesInspector = {
    ...attributesWrapper,
    ...attributesCustomize,
    ...attributesColor,
};

export default attributesInspector;
