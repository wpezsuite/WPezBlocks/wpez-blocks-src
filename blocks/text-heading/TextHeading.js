/**
 * External dependencies
 */
import map from "lodash/map";

import RichTextSingle from '../../components/edit/rich-text-single';

/**
 * Internal dependencies
 */
import config from "./config";

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Component, Fragment } = wp.element;

let thisConfig = {};

/**
 * Block edit function
 */
class TextHeading extends Component {

    constructor(props) {
        super(...arguments);

    }

    componentDidMount(){

        const {
            wpezBC,
        } = this.props;

        const thisBC = wpezBC.get('block__' + this.constructor.name);
        thisConfig = config(thisBC);

        const {
            attributes,
            setAttributes,
        } = this.props;

        // if an attribute is not set, then set it.
        map(thisConfig.initAttrs, (row, i) => {
            if (typeof attributes[row.attr] === 'undefined') {
                 setAttributes({[row.attr]: row.val });
             }
         });

    }

    render() {
     //   const blockName = 'text-heading';

        const {
            wpezBC,
        } = this.props;

        return (

            <Fragment>
                <RichTextSingle
                    blockName = {thisConfig.blockName}
                    rtsType = {thisConfig.rtsType}
                    wpezBC={wpezBC}
                    {...this.props}
                />
            </Fragment>
        );

    }
}

export default TextHeading;