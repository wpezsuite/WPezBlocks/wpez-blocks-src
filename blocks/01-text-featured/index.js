/**
 * External dependencies
 */
import classnames from 'classnames';
import map from 'lodash/map';

import ButtonGroupSizes from '../../components/attributes/button-group-sizes/';
import ButtonGroupBasicButtons from '../../components/attributes/base/base-button-group-basic-buttons/';
import ButtonGroupColorCombos from '../../components/attributes/button-group-color-combos/';
import ButtonGroupBlockWidth from '../../components/attributes/button-group-block-width/';
import ButtonGroupAlignHorizontal from '../../components/attributes/button-group-align-horizontal/';
import ButtonGroupAlignVertical from '../../components/attributes/button-group-align-vertical/';
import ButtonGroupTimeDuration from '../../components/attributes/button-group-time-duration/';
import ButtonGroupTimeDelay from '../../components/attributes/button-group-time-delay/';
import ButtonGroupHeading from '../../components/attributes/button-group-heading/';
import RangePickerWidthColumns from '../../components/attributes/range-picker-width-columns/';
import RangePickerAnimateIterations from '../../components/attributes/range-picker-animate-iterations/';
import SelectAniName from '../../components/attributes/select-animate-css';

// ---------------

import PanelBodySetup from '../../components/inspector-controls/panel-bodies/setup/panel-body';
import PanelBodyCustomizeRichText from '../../components/inspector-controls/panel-bodies/customize-rich-text/panel-body';
import PanelBodyAnimate from '../../components/inspector-controls/panel-bodies/animate/panel-body';



/**
 * Block dependencies
 */
import wpezUtils from '../../lib/js/wpez-utils';
import icon from './icon';
import './styles/style.scss';
import attributes from "./attributes";

const {
    Toolbar
} = wp.components;
const {
    withState
} = wp.compose;


/**
 * Internal block libraries
 */
const {
    __
} = wp.i18n;
const {
    registerBlockType
} = wp.blocks;
const {
    ColorPalette,
    FontSizePicker,
    PanelBody,
    PanelRow,
    TextareaControl,
    RadioControl,
    RangeControl
} = wp.components;
const {
    isSelected,
    RichText,
    PlainText,
    PanelColorSettings,
} = wp.editor;

const {
    AlignmentToolbar,
    BlockAlignmentToolbar,
    BlockControls,
    FormatToolbar,
    InspectorControls,
} = wp.blockEditor;
const {
    Fragment
} = wp.element;

const regBlockName = 'wpezblocks/text-featured';
/**
 * Register block
 */
export default registerBlockType(regBlockName, {
    title: __('Text: Featured', 'wpezblocks'),
    description: __('- TODO -', 'wpezblocks'),
    category: 'common',
    icon: {
        background: 'rgba(254, 243, 224, 0.52)',
        src: 'megaphone'
    },
    keywords: [
        __('TODO', 'wpezblocks'),
        __('TODO 1', 'wpezblocks'),
        __('TODO 2', 'wpezblocks')
    ],
    attributes,
    edit: props => {
        const wpez = wpezBlocks.textFeatured;
        let zzz = {};
        zzz.wpezTextFeature = wpez;
        const yyy= wpezBlocks;
        const {
            attributes: {
                blockAlignment,
                textAlignment,
                blockColor,
                //blockColor2,
                textFeatured,
                textSize,
                sizeButtons,
                blockButtons,
                colorCombos,
                option,
                myColumns
            },
            className,
            setAttributes
        } = props;

        let {
            attributes: {
                blockColor2
            }
        } = props;

        const DEFAULT_ALIGNMENT_CONTROLS = [{
            icon: 'editor-alignleft',
            title: __('Align text left'),
            align: 'left'
        }, {
            icon: 'editor-aligncenter',
            title: __('Align text center'),
            align: 'center'
        }, {
            icon: 'editor-alignright',
            title: __('Align text right'),
            align: 'right'
        }];

        function stringToBool(val) {

            return wpezUtils.stringToBool(val)
            //return (val === 'true') ? true : false
        }

        // let disableCustomColors = ( wpez.disableCustomColors === 'true') ? true : false

        let xxx = wpez.blockColors;

        var radioValue = '123';

        const myOpts = [
            {label: 'Author', value: 'a', color: 'blue'},
            {label: 'Editor', value: 'e', color: 'red'}
        ]

        const myCols = [
            {label: 'XXX', value: '1', color: 'blue'},
            {label: 'YYY', value: '2', color: 'red'}
        ]

        function onChangeRadio(value) {

            {
                radioValue: '123'
            }
            setAttributes({option: value});


        };


        function colorObject(colors, color, prop, undef = '') {

            return wpezUtils.colorObject(colors, color, prop, undef = '');

        }

        function myFN(opt) {

           // console.log(myOpts);
          //  console.log(map);
            return map(myOpts, (row, i) => {

                if (row['value'] == opt) {
                    return row['label'] + ' ' + row.color
                }
            })

            return myOpts[0].label

            return opt
        }


        const classes = classnames(
            className,
            'wpez-blocks wpez-blocks-text-featured', {
                [`block-align-${blockAlignment}`]: blockAlignment,
                [`text-align-${textAlignment}`]: textAlignment,
                [`block-color-${colorObject(wpez.blockColors, blockColor, "slug")}`]: blockColor,
                [`text-size-${sizeButtons}`]: sizeButtons,
                [`color-combo-${colorCombos}`]: colorCombos,
            });

        //   'blockAlignment': {blockAlignment},
        // textAlignment ? + 'text-align-' + textAlignment : '',
        // blockColor ? ' design-color-' + blockColor: '',

        const arrX = {};

        arrX.h6 = ['lg', 'a'];
        arrX.h5 = ['xs', 'e'];

        function onTextChange(changes) {
            setAttributes({
                'blockButtons': changes
            });
         //   console.log(changes);
            setAttributes({
                'sizeButtons': arrX[changes][0]
            });
            setAttributes({
                'option': arrX[changes][1]
            });
        }


        return (
            <div className={
                classes
            }>
                <h6> {__('mfs block 3', 'wpez-blocks')} </h6>

                <InspectorControls>

                    {true &&
                    <PanelBodySetup
                        wpezBlocks={wpezBlocks}
                        {...wpezBlocks}
                        {...props}
                    />
                    }

                    {true &&
                    <PanelBodyCustomizeRichText
                        {...wpezBlocks}
                        {...props}
                    />
                    }


                    {true &&
                    <PanelBodyAnimate
                        {...wpezBlocks}
                        {...props}
                    />
                    }

                    <PanelBody title={__("Layout", "wpezblock")}
                               initialOpen={true}
                               className={
                                   className + 'TODO-1 TODO-2'
                               }>


                        {false &&
                        <ButtonGroupHeading
                            label={__('Heading Tag C')}
                            value={blockButtons}
                            //     onChange={onTextChange}
                            //  onChange={value => setAttributes({blockButtons: value})}
                        />
                        }


                        {false &&
                        <ButtonGroupSizes
                            exclude={['xs', 'xl']}
                            label={__('123 Size Buttons')}
                            value={sizeButtons}
                            btnSize={'Xsm'}
                            btnSpacing={'Xsm'}
                            onChange={
                                value => setAttributes({
                                    sizeButtons: value
                                })
                            }


                        />
                        }
                        <p>1 - {radioValue}</p>
                        <p>2 - {myFN(option)} </p>
                        <RadioControl
                            label="User type"
                            help="The type of the current user"
                            selected={option}
                            options={myOpts}
                            // onChange = {value => setAttributes({option: value})}
                            onChange={onChangeRadio}

                        />

                        <ButtonGroupColorCombos {...this.props}
                                                XmyArr='123'
                                                label={
                                                    __('Color Combos')
                                                }
                                                orientation={
                                                    'portrait'
                                                }
                                                columns={5}
                                                value={
                                                    colorCombos
                                                }
                                                onChange={
                                                    value => setAttributes({
                                                        colorCombos: value
                                                    })
                                                }

                        />

                        {
                            wpez.blockAlignment == 'true' && wpez.blockAlignmentControlsTitle && (
                                <p>{wpez.blockAlignmentControlsTitle} </p>
                            )
                        }

                        {
                            wpez.blockAlignment == 'true' && (

                                <
                                    BlockAlignmentToolbar

                                    controls={
                                        wpez.blockAlignmentControls
                                    }
                                    value={
                                        blockAlignment
                                    }
                                    onChange={
                                        value => setAttributes({
                                            blockAlignment: value
                                        })
                                    }
                                />

                            )
                        }


                        {
                            wpez.textAlignment == 'true' && (<
                                    AlignmentToolbar alignmentControls={
                                    wpez.textAlignmentControls
                                }
                                                     value={
                                                         textAlignment
                                                     }
                                                     onChange={
                                                         value => setAttributes({
                                                             textAlignment: value
                                                         })
                                                     }
                                />

                            )
                        } <
                        /PanelBody> <
                        PanelBody title={
                        __("Design", "wpezblock")
                    }
                                  initialOpen={
                                      false
                                  }>

                        <
                            FontSizePicker fontSizes={
                            wpez.textSizes
                        }
                                           value={
                                               textSize
                                           }
                                           disableCustomFontSizes={
                                               stringToBool(wpez.disable_custom_font_sizes)
                                           }
                                           withSlider={
                                               stringToBool(wpez.with_slider)
                                           }
                                           fallbackFontSize={
                                               26
                                           }
                                           onChange={
                                               value => setAttributes({
                                                   textSize: value
                                               })
                                           }

                        />

                        <
                            p className="uagb-setting-label"> {
                            __("Separator Color")
                        }
                            < /p>

                                <
                                    ColorPalette colors={
                                    wpez.blockColors
                                }
                                                 disableCustomColors={
                                                     stringToBool(wpez.disableCustomColors)
                                                 }
                                                 value={
                                                     blockColor
                                                 }
                                    // onChange = { onChangeMessage}
                                                 onChange={value => setAttributes({blockColor: value})}
                                />


                                <
                                    p className="uagb-setting-label"> {
                                    __("Separator 222Color")
                                } < /p>

                                    <
                                        ColorPalette colors={
                                        xxx
                                    }
                                                     disableCustomColors={
                                                         stringToBool(wpez.disableCustomColors)
                                                     }
                                                     value={
                                                         blockColor2
                                                     }
                                                     onChange={
                                                         value => setAttributes({
                                                             blockColor2: value
                                                         })
                                                     }
                                    />


                                    <
                                        PanelColorSettings title="'Farbschema"
                                                           colorSettings={
                                                               [{
                                                                   colors: wpez.blockColors,
                                                                   label: 'Textfarbe',
                                                                   value: blockColor,
                                                                   onChange: value => setAttributes({
                                                                       blockColor: value
                                                                   })
                                                               },
                                                                   {
                                                                       colors: wpez.blockColors,
                                                                       label: 'Hintergrundfarbe',
                                                                       value: blockColor2,
                                                                       onChange: value => setAttributes({
                                                                           blockColor2: value
                                                                       })
                                                                   }
                                                               ]
                                                           }
                                    />


                                    <
                                        /PanelBody> <
                                        /InspectorControls>


                                        <TextareaControl
                                            formattingControls={wpez.formattingControls}
                                            tagName="div"
                                            multiline="p"
                                            pXlaceholder={
                                                __('Text: Featured', 'wpez-blocks')
                                            }
                                            onChange={
                                                (value) => setAttributes({
                                                    textFeatured: value
                                                })
                                            }
                                            value={textFeatured}
                                            inlineToolbar={false}
                                        />
            </div>

    );
    },
    save: props => {
        const {
        attributes: {
        textAlignment,
        textFeatured
    }
    } = props;
        return ( <
        div >
        <
        h2 > {
            __('Call to Action', 'wpez-blocks')
        } < /h2> <
        div class = "message-body" > {
            textFeatured
        } <
        /div> <
        /div>
        );
        }
        }, );


        const {
            addFilter
        } = wp.hooks;
        const {
            InspectorAdvancedControls
        } = wp.editor;
        const {
            createHigherOrderComponent
        } = wp.compose;
        const {
            ToggleControl
        } = wp.components;

        /**
         * Add mobile visibility controls on Advanced Block Panel.
         *
         * @param {function} BlockEdit Block edit component.
         *
         * @return {function} BlockEdit Modified block edit component.
         */
        const withAdvancedControls = createHigherOrderComponent((BlockEdit) => {

            return (props) => {

            const {
            attributes,
            setAttributes,
            isSelected,
        } = props;

            const {
            visibleOnMobile,
        } = attributes;


            return ( <
            Fragment >
            <
            BlockEdit {
                ...props
            }
            /> {
                isSelected &&
                <
                    InspectorAdvancedControls>
                    <
                        ToggleControl
                        onChange={
                            () => setAttributes({
                                visibleOnMobile: !visibleOnMobile
                            })
                        }
                        label={
                            __('Mobile Devices Visibity')
                        }
                        checked={
                            !!visibleOnMobile
                        }
                        help={
                            !!visibleOnMobile ? __('Showing on mobile devices.') : __('Hidden on mobile devices.')
                        }
                    /> <
                    /InspectorAdvancedControls>
                    }

                    <
                        /Fragment>
                        );
                        };
                        }, 'withAdvancedControls');

                        addFilter(
                        'editor.BlockEdit',
                        'editorskit/custom-advanced-control',
                        withAdvancedControls
                        );

                        var el = wp.element.createElement;

                        function removeCategoryPanel(OriginalComponent) {
                        return function (props) {
                        if (props.name === 'customClassName') {
                        return null
                    } else {
                        return el(
                        OriginalComponent,
                        props
                        );
                    }
                    }
                    };

                        wp.hooks.addFilter(
                        'editor.BlockEdit',
                        'editorskit/custom-advanced-control',
                        removeCategoryPanel
                        );
