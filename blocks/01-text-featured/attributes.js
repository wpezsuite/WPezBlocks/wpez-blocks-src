let wpez = wpezBlocks.textFeatured;

import attributesSetup from '../../components/inspector-controls/panel-bodies/setup/attributes';
import attributesCustomize from '../../components/inspector-controls/panel-bodies/customize-rich-text/attributes';
import attributesAnimate from '../../components/inspector-controls/panel-bodies/animate/attributes';



const attributes = {
    ...attributesAnimate,
    ...attributesSetup,
    ...attributesCustomize,

    textFeatured: {
        type: 'array',
        source: 'children',
        selector: '.message-body',
    },
    blockAlignment: {
        type: "string",
        default: wpez.blockAlignmentDefault
    },
    textAlignment: {
        type: "string",
        default: wpez.textAlignmentDefault
    },
    blockColor: {
        type: "string",
    },
    blockColor2: {
        type: "string",
        default: '#000000'
    },
    textSize: {
        type: "string"
    },
    option: {
        type: "string"
    },
    visibleOnMobile: {
        type: 'boolean',
        default: true,
    },
    horizontalFlip: {
        type: 'boolean',
        default: true,
    },
    verticalFlip: {
        type: 'boolean',
        default: false,
    },
    sizeButtons: {
        type: 'string',
        default: 'small'
    },

    blockButtons: {
        type: 'string',
        //  default: 'small'
    },

    colorCombos: {
        type: 'string',
    },
    myColumns: {
        type: 'string',
    },

};

export default attributes;
