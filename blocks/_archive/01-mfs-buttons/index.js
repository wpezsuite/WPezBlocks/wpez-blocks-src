/**
 * External dependencies
 */
import classnames from 'classnames';
import map from 'lodash/map';

/**
 * Internal dependencies
 */
import './styles/editor.scss';

/**
 * WordPress dependencies
 */
const { __, sprintf } = wp.i18n;
const { Component, Fragment } = wp.element;
const { Tooltip, ButtonGroup, Button } = wp.components;

/**
 * Responsive Controls Component
 */
class ButtonGroupSizes extends Component {

	constructor() {
		super( ...arguments );
	}

	render() {
		const {
			exclude,
			label,
			help,
			value,
			onChange,
			btngrpSizes
		} = this.props;

		const arrSizes = [
			{
				tooltipText: __('X-Small', 'wpez-blocks'),
				key: 'xs',
				buttonText: __('XS', 'wpez-blocks'),
			},
			{
				tooltipText: __('Small', 'wpez-blocks'),
				key: 'sm',
				buttonText:  __('S', 'wpez-blocks'),
			},
			{
				tooltipText: __('Medium', 'wpez-blocks'),
				key: 'md',
				buttonText: __('M', 'wpez-blocks'),
			},
			{
				tooltipText: __('Large', 'wpez-blocks'),
				key: 'lg',
				buttonText: __('L', 'wpez-blocks'),
			},
			{
				tooltipText: __('X-Large', 'wpez-blocks'),
				key: 'xl',
				buttonText: __('XL', 'wpez-blocks'),
			}
		];

		const slugBase = 'components-base-control';
		const slugClass = 'components-wpez-blocks-button-group-sizes';

		const classesWrapper = classnames(
			'wpez-blocks',
			'wpez-blocks-component',
			slugBase + '__wrapper',
			slugClass + '__wrapper', {}
		);

		const classesLabel = classnames(
			slugBase + '__label',
			slugClass + '__label', {}
		);

		const classesHelp = classnames(
			slugBase + '__help',
			slugClass + '__help', {}
		);

		const arrExclude = (exclude) ?  Array.isArray(exclude) ? exclude : [] : [];

		return (
			<Fragment>
				<div className={classesWrapper}>
				{ label && <span className={ classesLabel }>{ label }</span> }

				<ButtonGroup aria-label={ __( 'Select Size' ) }>
					{map(arrSizes, (row, i) => {
						if ( ! arrExclude.includes(row.key)){
							return (
								<Tooltip text={ row.tooltipText }>
								<Button
								key={ row.key }
								isLarge
								isPrimary={row.key === value ? true : false }
								aria-pressed={ !! btngrpSizes }
								onClick={ () => onChange( row.key ) }
								style={{textAlign: 'center', display: 'inline'}}
								>{row.buttonText}</Button>
								</Tooltip>
							)
						}
					})}
				</ButtonGroup>
				{ help && <p className={ classesHelp }>{ help }</p> }
				</div>
			</Fragment>
		);
	}
}

export default ButtonGroupSizes;
