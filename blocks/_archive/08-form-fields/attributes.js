const attributes = {

    checkboxControl: {
        type: 'boolean',
        default: true,
    },
    colorPaletteControl: {
        type: 'string',
        default: '#000000'
    },
    dateTimeControl: {
        type: 'string',
    },
    radioControl: {
        type: 'string',
        default: 'a',
    },
    rangeControl: {
        type: 'number',
        default: '10',
    },
    textControl: {
        type: 'string',
    },
    textareaControl: {
        type: 'text',
    },
    toggleControl: {
        type: 'boolean',
    },
    selectControl: {
        type: 'string',
    },
};

export default attributes;
