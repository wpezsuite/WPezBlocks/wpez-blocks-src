/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { Component } = wp.element;
const {
    ColorPalette,
} = wp.editor;
const {
    CheckboxControl,
    RadioControl,
    RangeControl,
    TextControl,
    TextareaControl,
    ToggleControl,
    SelectControl
} = wp.components;


/**
 * Create an Inspector Controls wrapper Component
 */
export default class Edit extends Component {

    constructor() {
        super( ...arguments );
    }

    render() {
        const {
            attributes: { checkboxControl, colorPaletteControl, radioControl, rangeControl, textControl, textareaControl, toggleControl, selectControl },
            className, setAttributes  } = this.props;

            const colors = [ 
                { name: 'red', color: '#f00' }, 
                { name: 'white', color: '#fff' }, 
                { name: 'blue', color: '#00f' }, 
            ];

            const colors2 = [ 
                { name: 'red', color: '#f00' }, 
                { name: 'blue', color: '#00f' }, 
                { name: 'white', color: '#fff' }, 
            ];

            console.log(wpezBlocks.color3);
            //console.log(wpezBlocks[]);

            var x = wpezBlocks.color3

            var colors3 = [];
            for(let y in x){
                console.log(y + x[y])
                colors3.push({name: y, color: x[y] })
            }
            console.log(wpezBlocks.color4)

            console.log( wpezBlocks.hasOwnProperty('color4'))
           console.log( wpezBlocks.hasOwnProperty('property1'))

        return (
            <div className={ className }>

                <CheckboxControl
                    heading={ __( 'Checkbox Control', 'jsforwpblocks' ) }
                    label={ __( 'Check here', 'jsforwpblocks' ) }
                    help={ __( 'Checkbox control help text', 'jsforwpblocks' ) }
                    checked={ checkboxControl }
                    onChange={ checkboxControl => setAttributes( { checkboxControl } ) }
                />

                <ColorPalette
                 colors={ colors } 
                    value={ colorPaletteControl }
                    disableCustomColors={true}
                    onChange={ colorPaletteControl => setAttributes( { colorPaletteControl } ) }
                />

<ColorPalette
                 colors={ wpezBlocks.color4 } 
                    value={ colorPaletteControl }
                    disableCustomColors={true}
                    onChange={ colorPaletteControl => setAttributes( { colorPaletteControl } ) }
                />

                <RadioControl
                    label={ __( 'Radio Control', 'jsforwpblocks' ) }
                    selected={ radioControl }
                    options={ [
                        { label: 'Author', value: 'a' },
                        { label: 'Editor', value: 'e' },
                    ]}
                    onChange={ radioControl => setAttributes( { radioControl } ) }
                />

                <RangeControl
                    beforeIcon="arrow-left-alt2"
                    afterIcon="arrow-right-alt2"
                    label={ __( 'Range Control', 'jsforwpblocks' ) }
                    value={ rangeControl }
                    onChange={ rangeControl => setAttributes( { rangeControl } ) }
                    min={ 1 }
                    max={ 10 }
                />

                <TextControl
                    label={ __( 'Text Control', 'jsforwpblocks' ) }
                    help={ __( 'Text control help text', 'jsforwpblocks' ) }
                    value={ textControl }
                    onChange={ textControl => setAttributes( { textControl } ) }
                />

                <TextareaControl
                    label={ __( 'Text Area Control', 'jsforwpblocks' ) }
                    help={ __( 'Text area control help text', 'jsforwpblocks' ) }
                    value={ textareaControl }
                    onChange={ textareaControl => setAttributes( { textareaControl } ) }
                />

                <ToggleControl
                    label={ __( 'Toggle Control', 'jsforwpblocks' ) }
                    checked={ toggleControl }
                    onChange={ toggleControl => setAttributes( { toggleControl } ) }
                />

                <SelectControl
                    label={ __( 'Select Control', 'jsforwpblocks' ) }
                    value={ selectControl }
                    options={ [
                        { value: 'a', label: 'Option A' },
                        { value: 'b', label: 'Option B' },
                        { value: 'c', label: 'Option C' },
                    ]}
                    onChange={ selectControl => setAttributes( { selectControl } ) }
                />

            </div>
        );
    }
}
