/**
 * Block dependencies
 */
import icon from './icon';
import meta from './meta';
import './style.scss';
import './editor.scss';

//console.log(meta);

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

var settings = {
  icon: {
    background: 'rgba(254, 243, 224, 0.52)',
    src: icon,
  },        
  keywords: [
      __( 'Banner', 'jsforwpblocks' ),
      __( 'CTA', 'jsforwpblocks' ),
      __( 'Shout Out', 'jsforwpblocks' ),
  ]
}
  
var settings2 = { 
  edit: props => {
    const { className, isSelected } = props;
    return (
      <div className={ className }>
        <h2>{ __( 'Edit: Static Call to Action', 'jsforwpblocks' ) }</h2>
        <p>{ __( 'This is really important!', 'jsforwpblocks' ) }</p>
        {
          isSelected && (
            <p className="sorry warning">{ __( '✋ Sorry! You cannot edit this block ✋', 'jsforwpblocks' ) }</p>
          )
        }
      </div>
    );
  },
}
 var settings3 = { 
   save: props => {
    return (
      <div>
        <h2>{ __( 'Save: Call to Action', 'jsforwpblocks' ) }</h2>
        <p>{ __( 'This is really important!', 'jsforwpblocks' ) }</p>
      </div>
    );
  },
}

/**
 * Register block
 */
export default registerBlockType(
    'jsforwpblocks/static',
    Object.assign(meta, settings, settings2, settings3)
);
