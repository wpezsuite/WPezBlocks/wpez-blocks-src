const { __ } = wp.i18n;

const meta = {  
    title:  __( 'Title 15', 'jsforwpblocks' ),
description: __( 'Demonstration of how to make a static call to action block.', 'jsforwpblocks' ),
category: 'common'
}

export default meta;
