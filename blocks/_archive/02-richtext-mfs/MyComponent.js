      import {Component} from 'wp.element';
      
      class MyComponent extends Component {

        constructor() {
            super( ...arguments );
        this.components = {
            foo: FooComponent,
            bar: BarComponent
        };
    }
        render() {
           const TagName = this.components[this.props.tag || 'foo'];
           return <TagName />
        }
    }

    export default MyComponent;