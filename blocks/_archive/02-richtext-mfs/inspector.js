/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { Component } = wp.element;
const {
  InspectorControls,
  ColorPalette,
  PanelColorSettings,
  ContrastChecker
} = wp.editor;

const {
  CheckboxControl,
  Button,
  ButtonGroup,
  Tooltip,
  PanelBody,
  PanelRow,
  RadioControl,
  RangeControl,
  TextControl,
  TextareaControl,
  ToggleControl,
  SelectControl
} = wp.components;

/**
 * Create an Inspector Controls wrapper Component
 */
export default class Inspector extends Component {
  constructor() {
    super(...arguments);
  }

  

  render() {
    const {
      attributes: {
        checkboxControl,
        colorPaletteControl,
        colorPaletteControl2,
        radioControl,
        rangeControl,
        textControl,
        textareaControl,
        toggleControl,
        selectControl
      },
      setAttributes
    } = this.props;

    const myFlag = true;

    return (
      <InspectorControls>
        <PanelBody
          title={__("Panel Body Title", "jsforwpblocks")}
          initialOpen={false}
        >
          <PanelRow>
            <p>{__("Panel Body Copy", "jsforwpblocks")}</p>
          </PanelRow>
        </PanelBody>


        <PanelBody>
          <CheckboxControl
            heading={__("Checkbox Control", "jsforwpblocks")}
            label={__("Check here", "jsforwpblocks")}
            help={__("Checkbox control help text", "jsforwpblocks")}
            checked={checkboxControl}
            onChange={checkboxControl => setAttributes({ checkboxControl })}
          />
        </PanelBody>
      

        <PanelColorSettings
          title={__("Color Settings", "jsforwpblocks")}
          colorSettings={[
            {
              value: colorPaletteControl,
              onChange: colorPaletteControl => {
                setAttributes({ colorPaletteControl });
              },
              label: __("Background Color")
            }
          ]}
        />


{ myFlag && (
        <PanelBody>
          <h3>{__("Color Settings 2", "jsforwpblocks")}</h3>
          <ColorPalette myFlag={true}
            value={colorPaletteControl2}
            onChange={colorPaletteControl2 => {
              setAttributes({ colorPaletteControl2 });
            }}
          />
          <ContrastChecker
            {...{
              // Text is considered large if font size is greater or equal to 18pt or 24px,
              // currently that's not the case for button.
              isLargeText: false,
              textColor: colorPaletteControl2,
              backgroundColor: colorPaletteControl
            }}
          />
        </PanelBody>
)}

<PanelBody>
<ButtonGroup>
        <Button>XS</Button>
        <Button>S</Button>
        <Button isPrimary>M</Button>
        <Button>L</Button>
        <Button value="xl" style={{backgroundColor: 'red' }}> ...</Button>
        <Tooltip text="tooltip text">
											<div className="components-coblocks-visual-dropdown__button-wrapper">
												<Button
                          key="key"
                          data-value="key"
													className="components-coblocks-visual-dropdown__button"
													isSmall
												>
													ICON
												</Button>
											</div>
										</Tooltip>
    </ButtonGroup>
</PanelBody>

        <PanelBody>
          <div class="radio-as-colors">
          <RadioControl
            label={__("Radio Control", "jsforwpblocks")}
            selected={radioControl}
            options={[
              { label: "Author", value: "a" },
              { label: "Editor", value: "e" }
            ]}
            onChange={radioControl => setAttributes({ radioControl })}
          />
          </div>
        </PanelBody>

        <PanelBody>
          <RangeControl
            beforeIcon="arrow-left-alt2"
            afterIcon="arrow-right-alt2"
            label={__("Range Control", "jsforwpblocks")}
            value={rangeControl}
            onChange={rangeControl => setAttributes({ rangeControl })}
            min={1}
            max={10}
          />
        </PanelBody>

        <PanelBody>
          <TextControl
            label={__("Text Control", "jsforwpblocks")}
            help={__("Text control help text", "jsforwpblocks")}
            value={textControl}
            onChange={textControl => setAttributes({ textControl })}
          />
        </PanelBody>

        <PanelBody>
          <TextareaControl
            label={__("Text Area Control", "jsforwpblocks")}
            help={__("Text area control help text", "jsforwpblocks")}
            value={textareaControl}
            onChange={textareaControl => setAttributes({ textareaControl })}
          />
        </PanelBody>

        <PanelBody>
          <ToggleControl
            label={__("Toggle Control", "jsforwpblocks")}
            checked={toggleControl}
            onChange={toggleControl => setAttributes({ toggleControl })}
          />
        </PanelBody>

        <PanelBody>
          <SelectControl
            label={__("Select Control", "jsforwpblocks")}
            value={selectControl}
            options={[
              { value: "a", label: __("Option A", "jsforwpblocks") },
              { value: "b", label: __("Option B", "jsforwpblocks") },
              { value: "c", label: __("Option C", "jsforwpblocks") }
            ]}
            onChange={selectControl => setAttributes({ selectControl })}
          />
        </PanelBody>
      </InspectorControls>
    );
  }
}
