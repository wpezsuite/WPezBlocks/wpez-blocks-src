/**
 * Block dependencies
 */

import Inspector from "./inspector";
import Controls from "./controls";
import attributes from "./attributes";

// import MyComponent from "./MyComponent";

import classnames from 'classnames';
import icons from './icons';
import './style.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const {
    registerBlockType,
} = wp.blocks;



const {
    RichText,
} = wp.editor;

/*
let y = ['TextControl',
    'Toolbar',
    'Button',
    'Tooltip',
    'PanelBody',
    'PanelRow',
    'FormToggle'];

    y.forEach(function(element, i) {
        console.log('Element', i, 'is', element);
   //    let z = wp.components[element];
       console.log(z);
      });
      */

const {
    TextControl,
    Toolbar,
    Button,
    Tooltip,
    PanelBody,
    PanelRow,
    FormToggle,
} = wp.components;


function getSettings(attributes) {
    let settings = [];
    for (let attribute in attributes) {
      let value = attributes[attribute];
      if ("boolean" === typeof attributes[attribute]) {
        value = value.toString();
      }
      if ("object" === typeof attributes[attribute]) {
        value = '123 456'; //value.toString();
      }
      settings.push(
        <li>
          {attribute} - {value} - {typeof attributes[attribute]}
        </li>
      );
    }
    return settings;
  }

/**
  * Register block
 */
export default registerBlockType(
    'jsforwpblocks/xinspector-controls',
    {
        title: __( 'Example 1234 - Inspector Controls', 'jsforwpblocks' ),
        description: __( 'An 123 example of how to use the Inspector component for a block.', 'jsforwpblocks'),
        category: 'common',
        icon: {
            background: 'rgba(254, 243, 224, 0.52)',
            src: icons.sidebar,
        },                 
        keywords: [
            __( 'Button', 'jsforwpblocks' ),
            __( 'Settings', 'jsforwpblocks' ),
            __( 'Controls', 'jsforwpblocks' ),
        ],
        attributes,
        getEditWrapperProps( attributes ) {
            const { blockAlignment } = attributes;
            if ( 'left' === blockAlignment || 
            'right' === blockAlignment || 
            'full' === blockAlignment ) {
                return { 'data-align': blockAlignment };
            }
        },
        edit: props => {
            const {
              attributes: { textAlignment, blockAlignment,  colorPaletteControl, colorPaletteControl2, message },
              attributes,
              className,
              setAttributes
            } = props;
            const onChangeMessage = message => { setAttributes( { message } ) };
            let settings = getSettings(attributes);
            return [
              <Inspector {...{ setAttributes, ...props }} />,
              <Controls {...{ setAttributes, ...props }} />,
              <div>
              <div className={className} style={{ textAlign: textAlignment }}>
                <ul>{settings}</ul>
              </div>
                              <div className={ className } style={{ backgroundColor: colorPaletteControl, color: colorPaletteControl2 }}>
                              <h2>{ __( 'Call to Action', 'jsforwpblocks' ) }</h2>
                              <RichText
                                  placeholder={ __( 'Add your custom message', 'jsforwpblocks' ) }
                                    onChange={ onChangeMessage }
                                    value={ message }
                                />
                          </div>
                          </div> 
            ];
          },
          save: props => {
            const {
              attributes: { textAlignment, className, blockAlignment, message },
              attributes
            } = props;
        
            let settings = getSettings(attributes);

            let x = JSON.stringify(props.attributes);
        
            return null
          }
    });