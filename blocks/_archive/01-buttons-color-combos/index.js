/**
 * External dependencies
 */
import classnames from 'classnames';
import map from 'lodash/map';

/**
 * Internal dependencies
 */
import './styles/editor.scss';
import icons from './icons';

/**
 * WordPress dependencies
 */
const { __, sprintf } = wp.i18n;
const { Component, Fragment } = wp.element;
const { RangeControl, TabPanel, Tooltip, ButtonGroup, Button, ColorIndicator } = wp.components;

/**
 * Responsive Controls Component
 */
class MyControl2 extends Component {

	constructor() {
		super( ...arguments );
	}

	render() {



		const {
			excludeSizes,
			label,
			help,
			myArr, 
			value,
			onChange,
			mybutton
		} = this.props;

		const labelClasses = classnames(
			'components-base-control__label',
			'components-coblocks-orientation__label', {}
		);

		const helpClasses = classnames(
			'components-base-control__help',
			'components-coblocks-orientation__help', {}
		);

		const myArgs = [
			{
				tooltipText: 'Zero',
				key: 'combo-0',
				isLarge: true,
				buttonColors: [ '#0083CA', '#007445','#FF0000', '#FFF7E1' ],
			},
			{
			tooltipText: 'One',
			key: 'combo-1',
			isLarge: true,
			buttonColors: [ '#007445','#FF0000', '#FFF7E1', '#0083CA' ],
		},
		{
			tooltipText: 'Two',
			key: 'combo-2',
			isLarge: true,
			buttonColors: [ '#FF0000', '#FFF7E1',  '#0083CA', '#007445' ],
		},
		{
			tooltipText: 'Three',
			key: 'combo-3',
			isLarge: true,
			buttonColors: [ '#FFF7E1',  '#0083CA', '#007445', '#FF0000' ],
		},
		{
			tooltipText: 'Four',
			key: 'combo-4',
			isLarge: true,
			buttonColors: 'XL',
		}
		];

		const onChangeClick = mybutton => { 

			setAttributes( mybutton );
	 
		 };

		 const arrExclude = (excludeSizes) ?  Array.isArray(excludeSizes) ? excludeSizes : [] : [];


		return (    
			<Fragment>
		
			 {(arrExclude) ? arrExclude : 'empty'}

				{ label && <span className={ labelClasses }>{ label }</span> }
				<div className="my-wrapper Xcomponents-coblocks-orientation">


					<ButtonGroup aria-label={ __( 'Select Orientation' ) }>

{map(myArgs, (row, i) => {   
	
	if ( ! arrExclude.includes(row.key) && Array.isArray(row.buttonColors)){
	
return (
	
	<Tooltip text={ row.tooltipText }>
	<Button
		key={ row.key }
		isLarge
		isPrimary={row.key === value ? true : false }
		aria-pressed={ !! mybutton }
		onClick={ () => onChange( row.key ) }
	//	style={{textAlign: 'center', display: 'inline'}}
	>
	{map(row.buttonColors, (color1, i) =>{
		return (
	<ColorIndicator 
	colorValue={color1}
	>
	</ColorIndicator>
		)}
	)}

	</Button>
	</Tooltip>

)
	}

	})}

					</ButtonGroup>
					{ help && <p className={ helpClasses }>{ help }</p> }
				</div>
			</Fragment>
		);
	}
}

export default MyControl2;
